\chapter{Inbetriebnahme und Messungen}\label{chap:inbetriebnahme-messungen}

Die im Folgenden beschriebenen Arbeitschritte werden mit den in \ref{tab:geraete} aufgeführten Geräten durchgeführt.
Aufgrund der hohen Spannungen werden sämtliche Messungen unter Aufsicht durchgeführt.
Zusätzlich wird ein Plexiglaskasten eingesetzt um Berührungsschutz herzustellen.

\begin{table}[h]
    \begin{center}
        \begin{tabular}{|l|l|}
            \hline
            Verwendungszweck & Hersteller \& Modell \\
            \hhline{|=|=|}
            Spannungsmessungen & Metraline DM41 \\
            \hline
            Strommessungen & Fluke 175 \\
            \hline
            DC Spannungsversorgung & Agilent Technologies N8742A \\
            \hline
            Trennstelltrafo & Thalheimer LTS602 \\
            \hline
            Oszilloskop & Rigol MSO5204 \\
            \hline
            Differenzieller Tastkopf & Micsig DP10007 \\
            \hline
            Strommesszange & Tektronix TCP2020A \\
            \hline
            Elektronische Last & RND 320-KEL103 \\
            \hline
            AC-Leistungsmessung & GW Instek GPM-8213 \\
            \hline
            Hochspannungstastkopf & Tektronix P5100A \\
            \hline
            Oszilloskop & Agilent Technologies DSO5054A \\
            \hline
            Differenzieller Tastkopf & Agilent Technologies N2790A\\
            \hline
        \end{tabular}
        \caption{Liste der benutzten Messgeräte}
        \label{tab:geraete}
    \end{center}
\end{table}

\section{Inbetriebnahme}\label{sec:inbetriebnahme}
Die erste Inbetriebnahme wird mit der DC-Versorgung durchgeführt, da diese eine feine Strombegrenzung zulässt.
So können Fehler gegebenenfalls erkannt werden bevor Bauteile thermisch zerstört werden.
Am Ausgang wird eine geringe Last von $\SI{50}{\milli\ampere}$ angelegt.
In diesem Betriebspunkt ist die Leistungsaufnahme begrenzt und
die Funktionsfähigkeit der Regelung kann besser erfasst werden als im Leerlauf.
Mit dem Oszilloskop wird die Spannung der Hilfswicklung überwacht.
Zusätzlich wird über den differenziellen Tastkopf die Drain-Source-Spannung am MOSFET überwacht.
Der differenzielle Tastkopf wird wegen seiner höheren Spannungsfestigkeit eingesetzt.

Unter Strombegrenzung wird die Betriebsspannung langsam erhöht.
Dabei wird ab $\approx\SI{30}{\volt}$ das in Abb. \ref{fig:Inb-VFault} gezeigte Verhalten sichtbar.
Die Erklärung ergibt sich aus dem Kapitel Fault Protection des Datenblatts \cite[S.19]{UCC28730}.
Die Zwischenkreisspannung wird während der Einschaltphase mit den Widerstand RS101 gemessen.
Kann der Strom durch RS101 einen Schwellwert nicht überschreiten,
so liegt die Betriebsspannung unterhalb des definierten Betriebsbereiches.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\linewidth]{Bilder/einmalFehler.png}
    \caption{Reglerverhalten bei zu geringer Spannung}
    \label{fig:Inb-VFault}
\end{figure}

Ab einem Schwellwert von $\ge \SI{100}{\volt}$ geht die Schaltung in den Regelbetrieb über.
Die Ausgangsspannung wird auf $\approx\SIrange{14,8}{15}{\volt}$ geregelt.
Damit ist die Grundfunktion der Schaltung gegeben.


\section{Wirkungsgrad}\label{sec:inb-wirkungsgrad}
Um sowohl Spannung als auch Last sicher erhöhen zu können wird währenddessen der Wirkungsgrad erfasst.

Die Messung des Wirkungsgrades erfolgt mit vier Multimetern, die jeweils Spannung und Strom an Eingang und Ausgang erfassen.
Dabei ist die Position der Amperemeter unerheblich,
die Voltmeter müssen allerdings so nah wie möglich am Anschluss der Schaltung erfolgen um
Abweichungen durch Leiter- und Übergangswiderstände vernachlässigen zu können.

Die Messreihen werden in \SI{50}{\volt} Schritten über den gesamten Lastbereich durchgeführt.
Sämtliche Messergebnisse sind in Anhang \ref{ap:wirkungsgrad-tabelle} zu finden,
Abb. \ref{fig:wirkungsgrad} zeigt den Verlauf in den wichtigsten Arbeitspunkten.

Dabei wird sowohl der Wirkungsgrad als $\eta = \frac{P_{out}}{P_{in}}$ ermittelt,
als auch die Differenz $\Delta P = P_{in} - P_{out}$.
Die Differenz ist ein guter Indikator für aufkommende Fehlfunktionen.
Denn $\Delta P$ beschreibt die Leistung, die als Verlust beziehungsweise Hitze in der Schaltung verbleibt.
Steigt diese unerwartet auf ein zu hohes Niveau kann schnell eingegriffen werden.

\section{Spannungsripple}

\emph{
    Ripple gering, einziger Ausschlag durch Schalten, ggf. EMI, aber unter/bei 1\%
}

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=0.8\linewidth]{Bilder/Ripple.png}
        \caption{Spannungsripple bei \SI{6}{\watt} Last}
        \label{fig:inb-ripple}
    \end{center}
\end{figure}


\section{Betrieb an Wechselstrom}\label{sec:inb-wechselstrom}
Da der Trennstelltransformator keine integrierte Strombegrenzung besitzt
muss der Spitzenwert von $\SI{325}{\volt}$ zuerst mit der DC-Versorgung angefahren werden.

Zur Messung an Wechselspannung kommt ein Leistungsmessgerät zum Einsatz, dass zusätzlich zu Effektivspannung und
-Strom auch den Leistungsfaktor $\lambda$ und die Wirkleistung erfassen kann.
Wie in \ref{sec:dim-acdc} erwartet fällt $\lambda$ mit $\approx 0,5$ sehr schlecht aus.
Die Begründung liegt in der pulsartigen Aufladung der Zwischenkreiskapazität,
die nur während der Spitzen der Versorgungsspannung erfolgt.
Die Stromspitzen sind in Abb. \ref{fig:inb-wechselStrom} zu sehen.

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=0.8\linewidth]{Bilder/wechselStrom.png}
        \caption{Eingangsstrom und $U_{Bulk}$ bei \SI{230}{\volt}/\SI{50}{\hertz}}
        \label{fig:inb-wechselStrom}
    \end{center}
\end{figure}

Da C117 während der Messungen nicht verbaut ist treten bei dieser Messung durch das Schalten des \acp{MOSFET} hochfrequente Störungen auf.
Sie werden für die Aufnahme per Durchschnittsbildung herausgefiltert.
Der Trennstelltransformator dient hier zusätzlich als Filter um andere, im Labor angeschlossene, Geräte nicht zu stören.

Wird nur die Wirkleistung betrachtet, so erreicht der Wandler bei \SI{7}{\watt} einen Wirkungsgrad von \SI{87}{\percent}.

\section{Spannungen über \SI{325}{\volt}}\label{sec:inb-hohe-spannung}
Da im Labor kein Trennstelltransformator für Dreiphasenwechselstrom vorhanden ist
können diese Spannungen sicher nur mit der DC-Versorgung getestet werden.
Die schrittweise Erhöhung der Versorgungsspannung zeigt eine Verringerung des Wirkungsgrades.
Die Verlustleistung bleibt aber unter \SI{2}{\watt}, einer Schwelle ab der der gewählte \ac{MOSFET} eine kritische Temperatur erreichen würde.
Es ist zu erwarten, dass der erzielte Wirkungsgrad von maximal \SI{80,5}{\percent} im realen Betrieb nicht erreicht werden kann,
da die schwankende Zwischenkreispannung und die entsprechenden höheren Spitzenströme den Wirkungsgrad drücken werden.



\subsection{Zwischenkondenatorspannung}
Das, in \ref{ssec:problematikDerHohenSpannung} erwähnte, Spannungsungleichgewicht zwischen den Zwischenkreiskondensatoren $U_{ZK}$ wird jetzt relevant.
Sobald die Zwischenkreispannung \SI{400}{\volt} überschreitet kann ein Kondensator überlastet werden.
Die Klemmung der Kondensatorspannungen wurde auf \SI{360}{\volt} festgelegt um das zu verhindern.
Die Funktion der Zenerdioden kann aber erst dann erkannt werden, wenn diese Spannung an einem Kondensator überschritten wird.
Die Messung von $U_{ZK}$ muss deshalb eng überwacht und sofort unterbrochen werden, falls eine Fehlfunktion der Klemmung erkannt wird.

Zusätzlich zur Gefahr der Zerstörung der Bauteile unterliegt die Überwachung von $U_{ZK}$ einer massiven Einschränkung:
Der maximale Leckstrom der Elektrolytkondensatoren wurde vom Hersteller mit $I_{LEAK,max} = \SI{81,6}{\micro\ampere}$ angegeben\cite{Wuerth-Cap}.
Bei einer Spannung von $U_{Clamp} = \SI{360}{\volt}$ ergibt sich ein sehr hoher parasitärer Innenwiderstand des Kondensators.

\begin{equation}
    R_{Innen} = \frac{U_{Clamp}}{I_{LEAK,max}} = \SI{4,4}{\mega\ohm}
    \label{eq:elko-Rinnen}
\end{equation}

$R_{Innen}$ ist so groß, dass die Parallelschaltung eines einfachen Voltmeters zu einer massiven Verzerrung des auftretenden Stroms führen würde.
Mit \SI{10}{\mega\ohm} ist der Innenwiderstand des Metraline Multimeters nur um Faktor $2$ größer, so senkt es den resultieren Widerstand um \SI{25}{\percent}.
Um eine annähernd realitätsnahe Messung durchzuführen wird ein Hochspannungstastkopf eingesetzt, der einen Innenwiderstand von \SI{40}{\mega\ohm} aufweist.
Somit kann der Einfluss auf die Schaltung auf den Faktor $10$ begrenzt werden.
Allerdings ist der Einfluss immernoch signifikant, der resultierende Strom liegt $\frac{1}{8} I_{LEAK,max}$ weit oberhalb der zu erwartenden Toleranz.

\begin{equation}
    I_{res} = \frac{U_{Clamp}}{R_{res}} = \SI{90}{\micro\ampere}
\end{equation}

Dies wird auch in der Messung sichtbar.
Während sich die Kondensatoren bei niedrigeren Spannungen langsam umladen bleibt die Differenz der Spannungen ab einem gewissen Punkt gleich.
Die Zenerdioden begrenzen sie auf \SI{360}{\volt}, die Schutzschaltung erfüllt ihren Zweck.
Das zeigt Abb. \ref{fig:Vbulk-balance}: $U_{Bulk}$ in grün und $U_{ZK}$ \SI{360}{\volt} tiefer in blau.

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=0.8\linewidth]{./Bilder/Vbulk-balance.png}
        \caption{Messung des Spannungsungleichgewichts}
        \label{fig:Vbulk-balance}
    \end{center}
\end{figure}

Außer der Funktion der Klemmung kann durch diese Messung allerdings
keine konkrete Aussage über die Ausgleichsschaltung getroffen werden.
Weder kann das reale Spannungsungleichgewicht, noch die tatsächliche Differenz der Leckströme erfasst werden.

Es lassen sich aber zwei grundlegendere Gedanken fassen.
Zum einen ist das Spannungsungleichgewicht von $\frac{2}{3}$ bei $U_Bulk = \SI{565}{\volt}$ recht groß
und die Spannungsbelastung eines Kondensators erheblich größer.
Dadurch wird die Lebensdauer asymmetrisch verkürzt, ein Kondensator wird früher ausfallend als nötig.
Eine niedrigere Klemmspannung von zum Beispiel \SI{300}{\volt} kann diesen Effekt verringern.
Zum anderen konnte gezeigt werden, dass ein paralleler Widerstand von \SI{40}{\mega\ohm} ausreicht um
die Toleranzen der Kondensatoren zu übersteuern.
Diese Alternative sollte in einer weiteren Iteration erneut betrachtet werden.

\subsection{Wirksamkeit der Snubberschaltung}

Abb. \ref{fig:Vmax-oszi} zeigt den Betrieb bei $U_{Bulk} = \SI{565}{\volt}$.
Für diese Messung kommen der differenzielle Tastkopf und das Oszilloskop von Agilent zum Einsatz,
da dieser Tastkopf Messungen bis \SI{1000}{\volt} erlaubt.

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=0.8\linewidth]{./Bilder/scope_0agil1.png}
        \caption{\acs{MOSFET}-Spannung bei maximaler Zwischenkreisspannung}
        \label{fig:Vmax-oszi}
    \end{center}
\end{figure}

Es ist zu erkennen, dass die schnelle Oszillation nach dem Ausschalten des \acp{MOSFET}
eine Amplitude von \SI{100}{\volt} nicht überschreitet.
Da die RCD-Clamp auf \SI{240}{\volt} ausgelegt wurde und $U_R$ bei $\SI{100}{\volt}$ liegt ist anzunehmen,
dass diese hier nicht oder nur begrenzt in Effekt tritt.
Das könnte auf eine niedrige Streuinduktivität des Speichertransformators hindeuten,
kann aber auch durch eine große Kapazität des \acp{MOSFET} begründet werden.

An der zweiten, langsamen Oszillation ist in Abb. \ref{fig:Vmax-oszi} zusätzlich zu sehen, dass die quasi-resonante Regelung
die Schaltspannung um etwas weniger als \SI{50}{\volt} senken kann.
Die QR-Regelung fällt bei höheren Spannungen immer weniger ins Gewicht, da die Amplitude $U_R$ der Resonanz konstant bleibt.

\section{Verteilung der Verluste}

\emph{
    grobes Wärmebild (Messung nach Ausschalten), MOSFET \textgreater\ Elko \textgreater\ Trafo\ \textgreater Snubber
}