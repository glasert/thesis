\chapter{Grundlagen}\label{chap:grundlagen}

\section{Anforderungen}\label{sec:anforderungen}
Die Anforderungen an die Schaltung werden aus dem Gesamtsystem des \ac{SST} abgeleitet.
Sie sind in Tabelle \ref{tab:anforderungen} zusammengefasst.
Die Ausgangsspannung $U_{out} = \SI{15}{\volt}$ wird zur Ansteuerung der \acsp{MOSFET}
beziehungsweise für deren DC/DC-Wandler benötigt.
Die Ausgangsleistung $P_{out} = \SI{7,5}{\watt}$ entspricht dem rechnerischen Bedarf des \ac{SST} und einer Leistungsreserve von \SI{0,7}{\watt}.
Der Eingangsspannungsbereich wird im \ac{RfP} \cite{requestForProposals} vorgegeben.
$U_{AC,min}$ entspricht einer optionalen Erweiterung der Standardanforderung.
Der hohe geforderte Wirkungsgrad erwächst aus dem in \ac{RfP} geforderten Gesamtwirkungsgrad von \SI{94}{\percent} bei \SI{300}{\watt}.
Von den zur Verfügung stehenden \SI{18}{\watt} nimmt die Hilfsspannungsversorgung mit bis zu \SI{9}{\watt} die Hälfte ein.
Die Anforderungen an Preis und Bauraum stammen aus der Teilnahme an der \ac{IFEC}.
So begrenzt das \ac{RfP} das Gesamtbudget auf lediglich 200 \$ 
und nennt den Bauraum alls entscheidendes Bewertungskriterium, sollte es einen Gleichstand geben.
Im geplanten modularen Design ist es sinnvoll, die Fläche auf die einer Halbbrücke zu begrenzen um sie mit diesen stapeln zu können.
Eine galvanische Trennung wäre für den Betrieb des \acp{SST} nicht zwangsläufig nötig,
erleichtert aber das Schaltungsdesign und gibt zusätzliche Sicherheit in der Spannungsfestigkeit.
\\
\begin{table}[h]
    \begin{center}
        \begin{tabular}{|l|l|}
            \hline
            Größe & Anforderung \\
            \hhline{|=|=|}
            niedrigste Eingangspannung $U_{AC,min}$ & \SI{120}{\volt}/\SI{60}{\hertz} \\
            \hline
            maximale Eingangspannung $U_{AC,max}$ & \SI{400}{\volt}/\SI{50}{\hertz} \\
            \hline
            Ausgangsleistung $P_{out}$ & \SI{7,5}{\watt} \\
            \hline
            Ausgangsspannung $U_{out}$ & \SI{15}{\volt} \\
            \hline
            Wirkungsgrad $\eta$ & $\ge \SI{85}{\percent}$ bei \SI{230}{\volt}/\SI{50}{\hertz} \\
            \hline
            Ausgangsspannungsripple $\Delta U_{out}$ & $\le \SI{1}{\percent} \cdot U_{out}$ \\
            \hline
            galvanische Trennung  & ja \\
            \hline
            maximaler Bauraum (L x B x H) & $\qtyproduct[product-units = single]{50 x 50 x 25}{\milli\meter}$ \\
            \hline
            Bauteilkosten bei 1000Stk. (ohne Platine) & \textless\ 15 \euro \\
            \hline
        \end{tabular}
        \caption{Anforderungen an das Netzteil}
        \label{tab:anforderungen}
    \end{center}
\end{table}


\section{Stand der Technik}\label{sec:stand-der-technik}
Auf dem Markt gibt es aktuell wenige Netzteile, die den weiten Spannungsbereich
bei der geforderten Leistung abdecken.
Der Anwendungsbereich solcher Netzteile ist schlichtweg begrenzt.
In den meisten Fällen und Geräten ist der Neutralleiter verfügbar.
Das Netzteil kann dann bei \SI{230}{\volt}/\SI{50}{\hertz} betrieben werden.
Das begrenzt die Spannungsanforderungen und erlaubt die Verwendung von Komponenten mit integriertem Leistungsschalter.

Auf den Anschluss zwischen zwei Außenleitern muss nur dort zurückgegriffen werden, wo kein Neutralleiter verfügbar ist.
%Die Anwendung bei \SI{400}{\volt} für den \ac{SST} ist also nicht ungewöhnlich, da dieser langfristig auch direkt am Netz agieren soll.
%Eine typische Stelle um vergleichbare Netzteile zu finden sind Drehstromzähler am Hausanschluss.

Das Modell {RAC15-15SK/480} der Firma RECOM erfüllt die Anforderungen weitgehend \cite{Recom-Supply}.
%Im Bezug auf seine Leistung ist es mit \SI{15}{\watt} erheblich überdimensioniert, der Wirkungsgrad ist aber höher als hier gefordert.
Der Wirkungsgrad ist hier höher als gefortert, im Bezug auf seine Leistung ist es aber mit \SI{15}{\watt} erheblich überdimentioniert.
Allerdings liegt der Preis mit 18,71 \euro\ bei Abnahme von 440 Stück oberhalb der geforderten 15 \euro\ pro Baugruppe.

Für die Teilnahme an der \ac{IFEC} dient dieses Netzteil als Rückfallebene, sollte diese Arbeit erfolglos verlaufen.
Um Kosten zu sparen ist es dennoch ratsam, diese Arbeit voran zu bringen und eine angepasste Lösung zu suchen.


\section{Auswahl der Topologie}
Während der Vorbereitungen auf diese Arbeit wurden verschiedene Lösungen untersucht.
Ein vielversprechender Ansatz bestand in einem Offline-Buck-Converter in Anlehnung an Application Note \cite{AN2872-HVBuck}
mit einem VIPer16L-Chip der Firma STMicroelectronics.
Der Vorteil dieser Schaltung besteht in ihrer Einfachheit, denn ihre Auslegung ist nur wenig anspruchsvoller als die eines regulären Tiefsetzstellers.
Allerdings bestehen zwei gravierende Nachteile:
zum einen kann keine galvanische Trennung von Ein- und Ausgang vorgenommen werden,
zum anderen ist nach \cite[Abschnitt~4.3]{AN2872-HVBuck} für höhere Spannungen nicht mit einem Wirkungsgrad oberhalb \SI{50}{\percent} zu rechnen,
was unter anderem auf die minimalen Einschaltzeiten des integrierten Leistungsschalters zurückzuführen ist.

\begin{equation}
    T_{on,Buck} = \frac{U_{out}}{U_{in}} \cdot T_{PWM} = \frac{\SI{15}{\volt}}{\SI{565}{\volt}} \cdot \frac{1}{\SI{60}{\kilo\hertz}} = \SI{442}{\nano\second}
    \label{eq:buck-Ton}
\end{equation}

Schlussendlich wurde der Sperrwandler als Topologie festgelegt.
Sein Aufbau erfordert trotz galvanischer Trennung eine minimale Anzahl an Komponenten.
Das begrenzt die Komplexität und damit auch die Kosten.
Durch die Popularität des Sperrwandlers ist zusätzlich eine Vielzahl von integrierten Ansteuerschaltungen verfügbar.
Durch den Gebrauch eines Speichertransformators zur Energieübertragung ist er zudem besonders gut für den weiten Spannungsbereich geeignet.
Im weiteren soll deshalb vertieft auf den Sperrwandler eingegangen werden.

\section{Sperrwandler Grundlagen}\label{sec:grundlagen-flyback}

Abb. \ref{fig:flybackSchema} zeigt den schematischen Aufbau eines Sperrwandlers
und soll für diesen Abschnitt als Referenz dienen.
Zu sehen sind dem Leistungsfluss nach zuerst Brückengleichrichter und Zwischenkreiskondensator $C_{Bulk}$.
Danach folgen der Speichertransformator mit dem Windungsverhältnis $N_{PS} = \frac{N_{Pri}}{N_{Sek}}$
und paralleler Snubberschaltung.
\ac{MOSFET} $Q_1$ dient als Leistungsschalter, der von einem Schaltregler angesteuert wird.
Auf der rechten Seite dann die galvanisch getrennte Sekundärwicklung mit Gleichrichterdiode und Glättungskondensator $C_{out}$
sowie die Hilfswicklung mit $N_{Aux}$ Windungen mit ähnlicher Beschaltung.
$N_{Aux}$ dient der Versorgung des Schaltreglers.
Die benötigte Leistung ist entsprechend gering.
Beim Sperrwandler können wie hier mehrere, auch isolierte, Ausgangskreise parallel betrieben werden.
Weil diese Windungen gleichzeitig leiten, können die Spannungen über die Verhältnisse eingestellt werden. 
Die Regelung, die hier nicht eingezeichnet ist, bezieht sich dabei typischerweise auf nur eine Spannung.

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=0.8\linewidth]{Bilder/BasicFlyback.pdf}
        \caption{Sperrwandler Grundschaltung}
        \label{fig:flybackSchema}
    \end{center}
\end{figure}
\subsection{Gleichrichter}

Als Gleichrichter kommt typischerweise ein Vollbrückengleichrichter zum Einsatz.
Gegenüber einem Ein-Dioden-Gleichrichter bietet er den Vorteil,
dass für die Ladung des Zwischenkreises die doppelte Anzahl an Halbwellen zur Verfügung steht.
So wird die Frequenz der Eingangswechselspannung verdoppelt und entsprechend die Periode des Auf- und Entladens von $C_{Bulk}$ halbiert.
Ebenfalls reduziert ist die Spannungsbelastung, da nie mehr als die Zwischenkreisspannung, also $U_P$, von den Dioden gesperrt werden muss.
Beim Einweggleichrichter mit einer Diode addiert sich die negative Halbwelle auf die Zwischenkreisspannung.
Die einzelne Diode muss dann $U_{PP} = 2 V_P$ blocken, was wiederum eine Reihenschaltung aus zwei Dioden erfordern kann.

\begin{equation}
    U_P = \sqrt{2} \cdot U_{AC,max}
    \qquad
    U_{PP} = 2 \cdot \sqrt{2} \cdot U_{AC,max}
\end{equation}

\subsection{Zwischenkreiskapazität}

Die Zwischenkreiskapazität $C_{Bulk}$ soll die gleichgerichtete Eingangsspannung so puffern,
dass der nachgelagerten Schaltung eine möglichst konstante Gleichspannung zur Verfügung steht.
Dafür kommen meist Elektrolytkondensatoren zum Einsatz.
Sie bieten einen guten Kompromiss zwischen Preis, Spannungsfestigkeit, Kapazität und Bauteilgröße.
Allerdings haben Elektrolytkondensatoren einen vergleichsweise großen Leckstrom $I_{Leak}$, der von Plattenfläche $A$, Plattenabstand $d$ und Betriebs\-spannung $U$ abhängig ist.%!Trennung
Seine Höhe wird von jedem Hersteller unterschiedlich angegeben.
Würth Elektronik gibt einen Maximalwert an \cite{Wuerth-Cap},
während beispielsweise TDK in \cite{TDK-Cap} die Formel (\ref{eq:tdk-cap-I-leak}) für das Maximum angibt.

\begin{equation}
    I_{Leak} = \SI{0,03}{\micro\ampere} \cdot (\frac{C_{Rated}}{\si{\micro\farad}} \cdot \frac{U_{Rated}}{U}) + \SI{15}{\micro\ampere}
    \qquad
    C_{Rated} = \varepsilon_0 \varepsilon_r \cdot \frac{A}{d}
    \label{eq:tdk-cap-I-leak}
\end{equation}

Hier ist der Zusammenhang der Größen zu erkennen.
Die Kapazität $C_{Rated}$ steht für die physischen Dimensionen des Kondensators.
An der Addition von $\SI{15}{\micro\ampere}$ ist abzuleiten,
dass es sich hier um eine sichere Abschätzung des Maximums handelt.
Eine genaue Bestimmung des Leckstroms ist im Vorhinein unter anderem deshalb nicht durchführbar, da dieser über die Lebensdauer zunimmt,
welche wiederum durch Stromwelligkeit, Spannungsbelastung und Temperatur beeinflusst wird.

\subsection{Speichertransformator}\label{ssec:grundlagen-trafo}

\subsubsection{Energieübertragung}

Das Konzept des Sperrwandlers beruht auf dem Speichern von Energie in einem Magnetfeld.
Dieses wird während der leitenden Phase auf der Primärseite aufgeladen und
während der sperrenden Phase auf der Sekundärseite entladen.

In regulären Transformatoren ist Energiespeicherfähigkeit unerwünscht, da diese die Energie direkt übertragen sollen.
Um Energie in entsprechender Größe speichern zu können, muss der Transformator also modifiziert werden.

% Ulrich "Diese Sätze klingen unverständlich oder zum Teil falsch. Überlegen Sie nochmal genau, was Sie aussagen wollen."
% Ulrich "Evtl. besser mit Formeln und Bildern arbeiten?"
%In einer Induktivität wirkt der Aufbau des magnetischen Flusses $\Phi$ dem Stromfluss $I$ entgegen.
%Dies geschieht bis zur Sättigung der Spule, wenn $B_{max}$ erreicht ist.
%Der magnetische Widerstand $R_m$ wirkt dem Aufbau des magnetischen Flusses entgegen.
%Ein reiner Eisenkern resultiert über seine hohe relative Permeabilität $\mu_r$ (vgl. Leitwert) in einem geringen Widerstand $R_m$.
%Das $\Phi$ kann sich fast ungehindert aufbauen und dem Strom so stark entgegenwirken.
%Die Induktivität ist hoch.
%Allerdings kann die Spule kaum Energie speichern.
%Der Sättigungsstrom $I_{sat}$ ist zu gering.
% Ulrich

Die Energie die in einem Magnetfeld gespeichert ist kann mit $E=I^2\cdot L$ bestimmt werden.
Zur Erhöhung der gespeicherten Energie kann also die Induktivität L vergrößert werden.
Diese wird allerdings durch die Schaltfrequenz beziehungsweise die maximale Ladedauer begrenzt.
Das geht aus \cite[Gleichung 7]{Infineon-DCM-2-8} hervor:

\begin{equation}
    L_{Pri,max} = \frac{V_{DCmin} \cdot D_{max}}{I_{pri} \cdot f_{sw,max}}
\end{equation}

Darüber hinaus kann eber der Strom $I$ erhöht werden.
Dieser wird durch die Sättigung des Transformatorkerns bei $B_{max}$ begrenzt.
Um bei gleichem magnetischen Fluss $B$ den Strom durch die Spule erhöhen zu können, muss der magnetische Widerstand $R_m$ des Kerns erhöht werden.
Dies geschieht durch das Einbringen eines Luftspaltes.
Der somit erhöhte magnetische Widerstand erlaubt einen höheren Sättigungsstrom $I_{sat}$ und damit eine verbesserte Energiespeicherfähigkeit.
Ein erhöhter magnetischer Widerstand senkt wiederum die Induktivität.
Dieser Zusammenhang wird über den $A_L$-Wert angegeben.
Er bezeichnet für einen Kern mit bestimmten Luftspalt die Induktivität pro Windung und wird vom Hersteller angegeben.


%Um diesen zu erhöhen muss die Feldstärke $H$ betrachtet werden.
%Diese ist proportional zum Spulenstrom $I$ und kann als magnetische Spannung $U_m$ betrachtet werden.
%
%Aufgrund des Zusammenhangs $\Phi=\frac{U_m}{R_m}$, der mit dem Ohmschen Gesetz vergleichbar ist, kann $I_{sat}$ über die magnetische Spannung erhöht werden.
%Um $\Phi$ konstant zu halten, muss dafür der Widerstand erhöht werden.
%$\Phi$ ist an jeder Stelle des magnetischen Kreises konstant,
%entsprechend kann ein zweiter magnetischer Widerstand in Reihenschaltung in den Kern eingebracht werden.
%
%Dieser nimmt die Form eines Luftspalts an.
%Der so erhöhte magnetische Widerstand $R_{m,Luft}$ erlaubt also einen höheren Sättigungsstrom und damit eine verbesserte Energiespeicherfähigkeit.
%Allerdings wirkt $R_m$ auch dem Aufbau des Magnetfelds entgegen wodurch die Induktivität verringert wird.


\subsubsection{Übersetzungsverhältnis} % Polarität hier?

Damit der Transformator überhaupt ein Magnetfeld aufbauen kann, muss verhindert werden,
dass während der Einschaltphase auf der Sekundärseite Strom fließt.
Dementsprechend muss die Spannung, die auf der Sekundärseite induziert wird, abgeblockt werden.
Dies wird mit einer Gleichrichterdiode erreicht, die in Reihe mit der Sekundärwicklung geschaltet wird.
Bei einem Wicklungsverhältnis von $N_{PS}=1$ müsste die Diode $U_{Bulk} + U_{out}$ sperren.
Mit einem höheren Windungsverhältnis kann die Spannungsbelastung auf der Sekundärseite gesenkt werden.
So wird es möglich, dort eine Schottkydiode mit verbesserten Schalteigenschaften zu nutzen.

Während der Ausschaltphase fließt der Strom dann auf der Sekundärseite und magnetisiert den Kern ab.
So wird wiederum eine Spannung auf der Primärseite induziert, man spricht von der reflektierten Spannung $U_R$.
Sie entspricht der Summe aus Ausgangsspannung und Vorwärtsspannung der Gleichrichterdiode,
die wiederum mit Kehrwert des Windungsverhältnises auftritt und vom \ac{MOSFET} geblockt wird.
Über eine Variation von $N_{PS}$ kann also die Verteilung der Spannungen eingestellt werden.
Die Gleichrichterdiode kann vom \ac{MOSFET} entlastet werden und andersherum.

\subsection{\acs{MOSFET}}

\begin{figure}[h]
    \begin{minipage}[c]{0.48\linewidth}
        \includegraphics[width=\linewidth]{Bilder/EinschaltPhase.pdf}
        \caption{Spannungen während der Einschaltphase}
        \label{fig:EinschaltPhase}
    \end{minipage}
    \hfill
    \begin{minipage}[c]{0.48\linewidth}
        \includegraphics[width=\linewidth]{Bilder/AusschaltPhase.pdf}
        \caption{Spannungenwährend der Ausschaltphase}
        \label{fig:AusschaltPhase}
    \end{minipage}
\end{figure}

Die maximale Spannung am \ac{MOSFET} liegt im Ausschaltmoment an.
Das ergibt sich aus der, in Abb. \ref{fig:AusschaltPhase} abgebildeten Masche.

\begin{equation}
    U_{Q,max} = U_{Bulk} + U_R + U_{Spike} = U_{Bulk} + U_{out} \cdot N_{PS} + U_{Spike}
    \label{eq:uQmaxTheo}
\end{equation}

Zur Sicherheit sollte auf die Durchschlagspannung des \acp{MOSFET} ein Puffer von $\approx\SI{30}{\percent}$ aufgeschlagen werden.
Darüber hinaus sollte eine Snubberschaltung eingesetzt werden.

\subsection{Snubberschaltung}

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=0.2\linewidth]{Bilder/NichtidealerTrafo.pdf}
        \caption{Ersatzschaltbild mit parasitären Größen}
        \label{fig:Lleak}
    \end{center}
\end{figure}

Der Speichertransformator besitzt wie jedes magnetische Bauteil mit mehreren Wicklungen eine Streuinduktivität $L_{Leak}$.
Diese kann, wie in Abb. \ref{fig:Lleak}, in Reihe mit der primären Transformatorwicklung betrachtet werden.
Sie beschreibt den Teil des Magnetfelds, der nicht mit der Sekundärwicklung gekoppelt ist und deshalb auch nicht von dieser abmagnetisiert werden kann.
Während sich $L_{Pri}$ über den idealen Transformator entladen kann, entwickelt $L_{Leak}$
im Ausschaltmoment eine Spannungsspitze $U_{Spike}$, um dem Abreißen des Stromflusses entgegen zu wirken.
Wird diese nicht begrenzt, kann sie den \ac{MOSFET} zerstören.
Eine erste Dämpfung geschieht durch die parasitäre Drain-Source-Kapazität des \acp{MOSFET}.
Dieser Schwingkreis resultiert in der ersten, schnellen Oszillation in Abb. \ref{fig:QR-Verlauf}.
Ist die in $L_{Leak}$ gespeicherte Energie allerdings zu groß, kann $U_{Spike}$ trotzdem zu groß werden.
In jedem Fall sollte aber die Oszillation gedämpft werden.

Dafür stehen verschiedene Snubberschaltungen zur Verfügung.
In diesem Fall soll die RCD-Clamp betrachtet werden.
Sie besteht - der Bezeichnung entsprechend - aus je einem parallelgeschalteten Widerstand und Kondensator in Reihe mit einer Diode.
Die RCD-Clamp wird parallel zur Primärwicklung und damit zur Leckinduktivität geschaltet.

Die Klemmfunktion der RCD-Clamp resultiert aus dem Zusammenspiel von Kapazität und Widerstand.
Der Strom der Streuinduktivität lädt $C_{Cl}$ auf, $U_{Spike}$ entspricht also $U_{Clamp}$.
$R_{Cl}$ bestimmt über die Zeitkonstante des RC-Glieds, wie weit sich $C_{Cl}$ bis zum nächsten Schaltvorgang entlädt.
Ist $U_{Spike}$ gering, so entlädt sich $C_{Cl}$ über die Schaltperiode kaum.
Die Klemmung tritt dann nicht in Kraft.
Bei höheren Spannungen fällt die Spannung durch die Entladung über $R_{Cl}$ so weit, dass $C_{Cl}$ wieder Energie aufnehmen kann.
\clearpage
\subsection{Quasi-Resonante Regelung}\label{ssec:grundlagen-qr}
In der Leistungselektronik ist es von großer Relevanz, Schaltverluste zu minimieren.
Eine Art Schaltverluste zu eliminieren ist das \acf{ZVS}, also das Schalten, wenn keine Spannung am Transistor anliegt.
Das kann mit der \acf{QR} Regelung nur in wenigen Fällen erreicht werden.
Eine Reduktion der Transistorspannung genügt allerdings schon, um die Schaltverluste zu verringern.

Grundlegend kann der Spannungsverlauf am \ac{MOSFET} im \ac{DCM} in drei Phasen unterteilt werden:

\begin{enumerate}
    \item Einschaltphase - Der \ac{MOSFET} leitet, es fällt keine Spannung ab, $L_{Pri}$ wird magnetisiert.
    \item Ausschaltphase - Der \ac{MOSFET} sperrt, die Sekundärseite leitet, $U_R$ addiert sich auf $U_{Bulk}$.
    \item passive Phase - Der Transformator ist entladen, \ac{MOSFET}-Spannung fällt auf $U_{Bulk}$ ab.
\end{enumerate}

Fällt die \ac{MOSFET}-Spannung nach dem Entladen der Sekundärseite auf $U_{Bulk}$ zurück,
so ist die Drain-Source-Kapazität $C_{DS}$ des \acp{MOSFET} noch mit $U_R$ geladen.
$C_{DS}$ entlädt sich jetzt über die Primärinduktivität $L_{Pri}$ des Speichertransformators.
So wird ein Schwingkreis gebildet, dessen Resonanz deutlich langsamer ist als die der ersten Oszillation mit $L_{Leak}$.
Beide sind in Abb. \ref{fig:QR-Verlauf} zu sehen.
Es ist zu erkennen, dass die Spannung nach der Entladung um $U_R = \SI{100}{\volt}$ auf $U_{Bulk} = \SI{100}{\volt}$ absinkt
und mit entsprechender Amplitude oszilliert.

%TODO Ulrich "Evtl. die Werte (UR,UBulk) und die 3 Phasen in das Diagramm einzeichnen, um es besser zu verstehen. (Siehe Abb.)"
\begin{figure}[h]
    \begin{center}
        \includegraphics[width=0.8\linewidth]{./Bilder/QR-Verlauf.png}
        \caption{Realer Verlauf der \acs{MOSFET}-Spannung}
        \label{fig:QR-Verlauf}
    \end{center}
\end{figure}


Das Konzept der \ac{QR} Regelung beruht nun auf dem Ausnutzen dieser Schwingung.
Durch das Schalten in deren Tiefpunkten kann die Schaltspannung um bis zu $U_R$ reduziert werden.
Ist $U_{Bulk}$ klein oder $U_R$ groß genug, wird auch \ac{ZVS} möglich.

Einfachere Schaltregler schalten immer im ersten Tiefpunkt und koppeln damit ihre Schaltfrequenz umgekehrt proportional an Eingangsspannung beziehungsweise Last.
Fortgeschrittene Regler wie der hier verwendete 'UCC28730' besitzen die Funktion des 'Valley Skipping', können also Tiefpunkte überspringen.
So kann nicht nur die Amplitude des Stroms, sondern auch die Frequenz moduliert werden.
Die Komplexität dieser Regelungsart ist zusätzlich erhöht,
da die Frequenz nur in den diskreten Schritten der Resonanzfrequenz variiert werden kann.


\subsection{Gleichrichterdiode und Glättung}

Die einfache Sekundärseite ist einer der Vorteile des Sperrwanders.
Sie besteht neben der Transformatorwicklung lediglich aus einer Gleichrichterdiode und einem Glättungskondensator.

Wie in \ref{ssec:grundlagen-trafo} beschrieben und in Abb. \ref{fig:EinschaltPhase} dargestellt muss die Diode die während der Einschaltphase induzierte Spannung sperren.
Sie wird typischerweise durch das Windungsverhältnis $N_{PS}$ reduziert.

\begin{equation}
    U_D = U_{Bulk} \cdot \frac{1}{N_{PS}} + U_{out}
    \label{eq:uDTheo}
\end{equation}

So kann eine Schottkydiode verwendet werden.
Diese hat keinen Reverse-Recovery-Effekt und kann deshalb erheblich schneller schalten.
Das ist hier notwendig, um dem Abmagnetisierungsstrom schnell einen Pfad zu öffnen.
Eine zu langsam schaltende Diode würde den Effekt einer vergrößerten Streuinduktivität annehmen
und eine vergrößerte Spannungsspitze am \ac{MOSFET} mit entsprechenden Verlusten erzeugen.

Der Glättungskondensator hat zwei Aufgaben:
er reduziert zum einen mit seiner Kapazität den Ausgangsripple, der durch die Schaltperioden entsteht,
zum anderen soll er hochfrequente Störungen, die durch Schalten und Resonanzen entstehen, filtern.
Für diese Fähigkeit ist die \ac{ESR} des Kondensators ausschlaggebend.
Ist diese wie bei Elektrolytkondensatoren recht groß, liegt die Grenzfrequenz im Rahmen des Ripples.
Durch einen parallelen Keramikkondensator kann eine verringerte \ac{ESR} bei gleichzeitig großer Kapazität erreicht werden.