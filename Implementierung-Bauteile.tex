\chapter{Implementierung}

\section{Bauteile}

\subsection{Gleichrichter und Zwischenkreis}\label{ssec:grundlagen-gleich-zwischen}

Als Gleichrichter sollte ein Vollbrückengleichrichter zum Einsatz kommen.
Gegenüber einem Ein-Dioden-Gleichrichter bietet er den Vorteil,
dass für die Ladung des Zwischenkreises die doppelte Anzahl an Halbwellen zu Verfügung steht.
Ebenfalls ist die Spannungsbelastung reduziert, da nie mehr als die Zwischenkreisspannung, also $\hat{V}_{AC,max}$ geblockt werden muss.
Beim Ein-Dioden-Gleichrichter addiert sich die negative Halbwelle auf die Zwischenkreisspannung.

Als Pufferkondensator $C_{Bulk}$ werden klassischer Weise Elektrolytkondensatoren verwendet.
Sie bieten, verglichen beispielsweise mit Folienkondensatoren, eine größere Kapazität bei kleinerem Bauraum.
Da die die Zwischenkreisspannung mit $\hat{V}_{AC,max} = \SI{565}{\volt}$ oberhalb der maximalen Betriebsspannung
gängiger Elektrolyt-Kondensatoren liegt müssen weitere Schritte ergriffen werden.
Um die Spannungsanforderung zu erfüllen werden zwei Kondensatoren in Reihe geschalten.
Diese Anordnung hat bei hohen Spannungen allerdings eine Schwachstelle.
Elektrolytkondensatoren haben einen Leckstrom.
Dieser ist Proportional zu Plattenfläche und Betriebsspannung.

§Gleichung/Schaubild§

Da die Größe der Plattenfläche mit der Kapazität einhergeht
§
wird in den meisten Datenblättern folgende Gleichung für die Ermittlung des Leckstroms angegeben.
§
§
Wäre der Leckstrom so exakt zu bestimmen und einzustellen könnte hier nach der Erwähnung der Verluste Schluss sein.
Allerdings ist das Ergebnis für wenig mehr als eine Abschätzung der Größenordnung zu gebrauchen.
Der Leckstrom unterliegt einer großen Toleranz, die nicht angegeben werden kann.

Durch unterschiedliche Leckströme in den Kondensatoren kann es nun zu einem Ladungsungleichgewicht kommen.
Ist zum Beispiel der Leck von C1 erheblich größer als der von C2, so sammelt sich in C2 mit der Zeit eine immer größere Ladung.
Hierdurch steigt auch die Spannung über C2.
Kann die erhöhte Spannung das Ungleichgewicht der Leckströme nun nicht kompensieren, so steigt die Spannung über den zulässigen Bereich hinaus.
Hierdurch kommt es mindestens zu einer stark verkürzten Lebenszeit, im schlimmsten Fall aber auch zum Versagen des Kondensators.

Um diesem Fall entgegen zu wirken gibt es zwei Möglichkeiten.
Zum einen kann über einen Spannungsteiler die Spannung am mittleren Knoten eingestellt werden.
Um ein Abfließen unausgeglichener Leckströme zu gewährleisten sollte durch den Spannungsteiler ein 5- bis 10-Faches des Leckstroms fließen.
Ebenfalls ist die Spannungsfestigkeit der Widerstände zu berücksichtigen.
Die andere Möglichkeit ist, die Spannungen der einzelnen Kondensatoren über Zenerdioden zu klemmen.
Dabei kann sich zwar in den Kondensatoren ein Ungleichgewicht aufbauen, dieses wird aber auf ein sicheres Niveau begrenzt.
Hierbei entstehen im Idealfall keine zusätzlichen Verluste, allerdings sind Zenerdioden kostenintensiver als Widerstände.
Die Toleranz der Zenerdioden ist zu beachten.
§cite Buck-Paper§

Die Spannweite der Zwischenkreisspannung $V_{Bulk}$ kann wie folgt berechnet werden.
Abb. \ref{fig:V-Bulk} und (\ref{eq:V_DC,min}) stammen aus §cite Infineon-QR-Paper§.
Das Maximum $V_{DC,max}$ ergibt sich aus dem Spitzenwert der gleichgerichteten Netzspannung.
\begin{equation}
    V_{DC,max} = \hat{V}_{AC,max} = V_{AC,max} \cdot \sqrt{2}
    \label{eq:V_DC,max}
\end{equation}
Der Minimalwert der Zwischenkreisspannung $V_{DC,min}$ ist davon abhängig wie weit der Pufferkondensator $C_{Bulk}$ während einer Halbwelle entladen wird.
Umso größer $C_{Bulk}$ ist, umso stabiler ist $V_{Bulk}$ und umso weniger bricht die Spannung während des Entladens ein.
Ein entsprechender Verlauf ist in Abb. \ref{fig:V-Bulk} abgebildet.
$V_{DC,min}$ kann mit folgender Formel ermittelt werden.
Der Ladezyklus $d_{charge}$ kann dabei mit $0,2$ angenähert werden. $P_{in,max}$ entspricht dem Quotienten aus maximaler Nutzleistung und Wirkungsgrad.

\begin{equation}
    V_{DC,min} = \sqrt{2 \cdot V_{AC,min} ^ 2 - \frac{P_{in,max} \cdot (1 - d_{charge})}{C_{Bulk} \cdot f_{AC}}}
    \label{eq:V_DC,min}
\end{equation}

In Abb. \ref{fig:V-DCmin} wird die Minimalspannung in Abhängigkeit der von $C_{Bulk}$ betrachtet.
Es ist zu erkennen, dass eine Erhöhung der Kapazität im niedrigen Bereich einen großen Effekt hat,
der nach einem Optimum stark abnimmt.

\begin{figure}
    \begin{minipage}[c]{0.4\linewidth}
    \includegraphics[width=\linewidth]{./Bilder/VBulk-minmax.png}\label{fig:V-Bulk}
    \caption{Ermittlung von maximaler und minimaler Zwischenkreisspannung}
    \end{minipage}
    \hfill
    \begin{minipage}[c]{0.4\linewidth}
    \includegraphics[width=\linewidth]{./Bilder/VDCminPlot.png}\label{fig:V-DCmin}
    \caption{Beispielhafter Verlauf von $V_{DC,min}$ in Abhängigkeit von $C_{Bulk}$}
    \end{minipage}
\end{figure}

Im Datenblatt des Schaltreglers §S.23(9)§ ist eine weitere Gleichung angegeben um wahlweise Spannungsminimum oder die benötigte Kapazität auszurechnen.
Hier wird ein zusätzlicher Faktor $N_{HC}$ verwendet um ausfallende Halbwellen der Versorgungsspannung zu berücksichtigen.

\begin{equation}
    C_{Bulk} \geq \frac{2 P_{in,max} \cdot (0,25 + 0,5 N_{HC} + \frac{1}{2\pi} \cdot \arcsin(\frac{V_{DC,min}}{\sqrt{2} \cdot V_{AC,min}}))}
                        {(2 V_{AC,min}^2 - V_{DC,min}^2) \cdot f_{AC}}
\end{equation}

Wird $N_{HC}$ vernachlässigt, so ist die gewählte Kapazität
$C_{Bulk} = \SI{6,8}{\micro\farad}$ ausreichend um eine Mindestspannung von
$V_{DC,min} = \SI{90}{\volt}$ zu gewährleisten.
Da der SST fest installiert werden soll ist diese Vereinfachung möglich.





\section{Speichertransformator}\label{sec:trafo-grundlagen}
Dieser Abschnitt orientiert sich stark an §cite§2-9 und 2-8.


Ist der Spannungsbereich definiert sollte über die Spannungsbelastung des
Transistors zunächst die durch die Sekundärwicklung reflektierte Spannung $V_R$ festgelegt werden.
Die größte Spannung, die am MOSFET liegt unmittelbar nach dem Ausschalten an.
In diesem Moment addieren sich die Zwischenkreisspannung $V_{Bulk}$, $V_R$ und
die eine Spannungsspitze $V_{spike}$, die durch die Leckinduktivität verursacht wird.
Diese kann zunächst mit \SI{30}{\percent} der Maximalspannung des MOSFETs angenommen werden.
Im weiteren Entwicklungsverlauf kann $V_{spike}$ durch eine Snubberschaltung (siehe §cite snubber section§) begrenzt werden.

\begin{equation}
    V_{DS,max} = V_{DC,max} + V_R + V_{spike} \label{eq:V-DSmax}
\end{equation}

Die reflektierte Spannung hat aber nicht nur Einfluss auf die Belastung des MOSFETs,
sondern auch auf die der sekundärseitigen Gleichrichterdiode.
Kurz gefasst: Je mehr eine Seite belastet wird, umso mehr wird die andere entlastet.
Mehr dazu in §cite sec rect diode section§.

Bezogen auf die Regelung hat $V_R$ zwei Implikationen.
Zum Einen ist der Duty-Cycle ist von $V_{Bulk}$ und $V_R$ abhängig.
Der größte Duty-Cycle tritt bei der kleinsten Spannung $V_{DC,min}$ auf, da der Speichertransformator
für die gleiche Energie länger geladen werden muss.
Damit der Wandler unterhalb der Grenze vom DCM in den CCM bleibt sollte der maximale Duty-Cycle $D_{max}$ begrenzt werden.
Es ist anzumerken, dass die Berechnung von $D_{max}$ im Quasi-Resonanten Modus abweicht, siehe (\ref{eq:DmaxQR}).

\begin{equation}
    D_{max,DCM} = \frac{V_R}{V_R + V_{DC,min}}
    \label{eq:DmaxDCM}
\end{equation}

Zum Anderen beschreibt sie die Tiefe der Spannungssenke nach dem Entladen des Trafos.
Nach dem Entladen fällt die Spannung am MOSFET von $V_{Bulk} + V_R$ auf $V_{Bulk}$ ab.
Dabei entsteht eine Oszillation.
Wird diese nicht gedämpft entspricht der erste Tiefpunkt $V_{Bulk} - V_R$.
Dieses Verhalten wird von Quasi-Resonanten Reglern verwendet um eine möglichst geringe Einschaltspannung zu erreichen.
Siehe §cite QR section§.
An dieser Stelle ist es möglich $V_R$ so weit zu erhöhen, dass die erste Senke auf oder sogar unter Null sinkt,
so dass Zero Voltage Switching erreicht werden kann.
Hier muss allerdings wieder auf den Beginn des Abschnitts verwiesen werden, die Spannung am MOSFET ist begrenzt.
Die gesparten Einschaltverluste konkurrieren hier mit den Verlusten des Snubber zur Senkung von $V_{spike}$
beziehungsweise den Kosten für einen MOSFET mit höherer Spannungsfestigkeit.
§Notiz an die Welt: Eine Gegenüberstellung Snubber-Energie vs Einschaltverluste wäre sehr interessant.§

\subsection{IPri,peak und LPri}
Als nächstes können nur der maximale Primärstrom und die Primärinduktivität berechnet werden.
Die Höhe des Spitzenstroms ändert sich nicht mit der Höhe der Zwischenkreisspannung, da der Duty-Cycle entsprechend geregelt wird.
Für die Berechnung wird der bereits bekannte Betriebspunkt bei niedrigster Spannung und größtem Duty-Cycle verwendet.
Für Eingangsleistung wird ein Wirkungsgrad $\eta \approx \SIrange{80}{85}{\percent}$ angenommen, es gilt $P_{in,max} = \frac{P_{out,max}}{\eta}$.

\begin{equation}
    I_{Pri,peak} = \frac{2 \cdot P_{in,max}}{V_{DC,min} \cdot D_{max}}
\end{equation}

Die Primärinduktivität muss nun so gewählt werden, dass $I_{Pri,peak}$ in jedem Takt erreicht werden kann.
Eine zu große Induktivität hätte zur Folge, dass der Wandler in den CCM übergeht.
Der Maximalwert um bei voller Last im DCM zu verbleiben kann wie folgt bestimmt werden.

\begin{equation}
    L_{Pri,max} = \frac{V_{DC,min} \cdot D_{max}}{I_{Pri,peak} \cdot f_{sw,max}}
    \label{eq:L-Primax}
\end{equation}

Im Quasi-Resonanten Modus muss zusätzlich sichergestellt werden, dass die erste Spannungssenke innerhalb des Schaltzyklus liegt.
Es ergibt sich ein weiteres Maximum für die Primärinduktivität, das oberhalb $L_{Pri,max}$ liegen sollte.
Wobei $f_{sw,min}$ niedrigste Schaltfrequenz des QR-Schaltreglers und die $C_D$ die Drain-Kapazität des MOSFETs, näherungsweise $C_{oss}$ darstellt.

\begin{equation}
    L_{Pri,max,QR} = \frac{1}{(\sqrt{2 \cdot P_{in,max} \cdot f_{sw,min}} \cdot (\frac{1}{V_{DC,min}} + \frac{1}{V_R}) + \pi \cdot f_{sw,min} \cdot \sqrt{C_D})^2}
    \label{eq:L-PrimaxQR}
\end{equation}

Im Quasi-Resonanten Modus ist die Dauer des dritten Schaltzustands (vgl. §ref QR Grundlagen) im Gegensatz zum DCM nicht beliebig.
Sie wird über die Resonanzfrequenz von Primärinduktivität und Drain-Gate-Kapazität des MOSFETs vorgegeben.
Deshalb liegt $D_{max,QR}$ unterhalb von $D_{max,DCM}$.
Hier sollte iterativ vorgegangen werden 

\begin{equation}
    D_{max,QR} = 
    \label{eq:DmaxQR}
\end{equation}






Sekundärseite
Während die Primärinduktivität geladen wird wird in den anderen Wicklungen eine Spannung induziert.
Diese muss abgeblockt werden.

Während der MOSFET eingeschaltet ist liegt über dem Trafo $V_Bulk$ an.
Entsprechend dem Windungsverhältnis $N_{PS}$  wird auf der Sekundärseite eine Spannung induziert.
Beim Sperrwandler muss diese gesperrt werden.
Zusätzlich zur induzierten Spannung liegt an der Diode auch die Ausgangsspannung an.
Die Spannungsbelastung ergibt sich wie folgt.

\begin{equation}
    V_{REV} = V_{DC,max} * N_{PS} + V_{out}
    \label{eq:Vrev}
\end{equation}

Hier ist eine Schottkydiode zu empfehlen, da diese schnell einschalten kann wodurch zusätzliche Spannungspitzen auf der Primärseite vermieden werden.



%Implementierung
\chapter{Implementierung}

Bei allen Teilen der tatsächlichen Schaltung muss das Datenblatt des Schaltreglers berücksichtigt werden.
Unter \glqq Application and Implementation\grqq § werden Hinweise und Formeln zur Auslegung der Bauteile gegeben.
Entsprechend ist dieses Kapitel stark an den Abschnitt \glqq Typical Application\grqq angelehnt.

\section{Gleichrichter und Zwischenkreis}

Als Gleichrichter kommt ein Vollbrückengleichrichter zum Einsatz.
Eine aktive Schaltung wäre für die geringe Leistung übertrieben und würde den Bauraum vergrößern.

Für die Zwischenkreiskapazität $C_{BULK}$ wird im Datenblatt folgende Formel angegeben.
Der Sinn der Rechnung entspricht dem aus \ref{ssec:grundlagen-gleich-zwischen}:
Die Zwischenkreisspannung $V_{BULK}$ muss oberhalb der Spannung $V_{BULK (min)}$ bleiben,
so dass die Primärinduktivität während der Einschaltzeit ausreichend aufgeladen werden kann.
$V_{BULK (min)}$ kann dabei frei gewählt werden.
Durch Ausprobieren beziehungsweise Plotten kann mit der Formel auch $V_{BULK (min)}$ für eine gegebene Kapazität ermittelt werden.
Der zusätzliche Faktor $N_{HC}$ berücksichtigt ausfallende Halbwellen.
Er wird nicht berücksichtigt, da von einer festen Installation und stabiler Versorgungsspannung ausgegangen werden kann.
Für $V_{IN(min)}$ wird der Effektivwert der niedrigsten Spannung eingesetzt, $f_{LINE}$ ergibt sich entssprechend.

\begin{equation}
    C_{BULK} \ge \frac  {2 \cdot P_{IN} \cdot (0,25 + 0,5 N_{HC} + \frac{1}{2\pi}(\frac{B_{BULK (min)}}{\sqrt{2} \cdot  V_{IN(min)}}))}
                        {(2 V_{IN(min)}^2 - V_{BULK (min)}^2) \cdot f_{LINE}}
\label{eq:impl-Cbulk}
\end{equation}

Aufgrund des Bestellstopps der Hochschule mussten die Bauteile bereits vor der exakten Auslegung der Schaltung bestellt werden.
Deshalb werden, auch im Vergleich zu Tabelle §XXXXXXXXX§, verhältnismäßig kleine Kondensatoren mit $\SI{6,8}{\micro\farad}$ verwendet.
Bei $V_{IN(min)} = \SI{120}{\volt}$ ergibt sich mit (\ref{eq:impl-Cbulk}) eine Zwischenkreisspannung von §XXXXXXXXXXXXXXX§.
Es ist wichtig zu betonen, dass die Versorgungsspannung des Hilfsnetzteils mit steigender Last am SST zusätzlich absinken wird.
Tests müssen zeigen ob ein stabiler Betrieb unter den entsprechenden Bedingungen möglich ist oder ob $C_{BULK}$ vergrößert werden muss.
Wie in Abb. \ref{fig:V-Bulk} zu sehen ist kann in diesem niedrigen Bereich schon eine kleine Erhöhung der Kapazität eine Verbesserung bringen.

\section{Speichertransformator}
Bei der Auslegung gelten die selben Prinzipien und Grenzen aus \ref{sec:trafo-grundlagen}.
Es werden allerdings weiterhin zuerst die Angaben aus dem Datenblatt herangezogen.

Zuerst wird hier der maximale Duty-Cycle $D_{MAX}$ berechnet.
Er wird durch die maximale Entmagnetisierungszyklus von $D_{MAGCC} = 0,432$ und einer Halbwelle der Resonanten Schwingung begrenzt.
Für deren Abschätzung wird eine Frequenz von $\SI{500}{\kilo\hertz}$ empfohlen, was einer Periodendauer von $t_R = \SI{2}{\micro\second}$.
Sie wird mit der maximalen Schaltfrequenz des Reglers von $f_{MAX} = \SI{83,3}{\kilo\hertz}$ normiert.

\begin{equation}
    D_{MAX} = 1 - D_{MAGCC} - (\frac{t_R}{2} \cdot f_{MAX}) = 0,485
    \label{eq:impl-Dmax}
\end{equation}

Nun kann das Windungsverhältnis $N_{PS(ideal)}$ berechnet werden.
Ideal deshalb, weil eine Abweichung nach oben XXXXXXXXXXXXXXXX§
Für die Spannung der Sekundärwicklung wird die Summe aus
$V_{OCV}$, der regulären Ausgangsspannung (output constant voltage),
$V_F$, der Vorwärtsspannung der Gleichrichterdiode und
$V_OCBC$, einem Spannungsoffset zur Kompensation von Leiterverlusten
gebildet.
$V_OCBC$ wird im Weiteren nicht berücksichtigt,
eine spätere Anpassung wäre mittels eines Widerstands möglich um die Ausgangsspannung leicht zu erhöhen.

\begin{equation}
    N_{PS(ideal)} = \frac{D_{MAX} \cdot V_{BULK(min)}}{D_{MAGCC} \cdot ( V_{OCV} + V_F + V_{OCBC})}
    \label{eq:NpsIdeal}
\end{equation}

Aufgrund der internen Reglerstruktur des ICs wird der Spitzenstron $I_{PP}$ über den
Stommesswiderstand $R_{CS}$ bestimmt.
Dieser begrenzt des Ausgangsstrom auf $I_{OCC}$ (output constant current).
Er wird über das Windungsverhältnis auf der Primärseite sichtbar und mit $R_{CS}$ gemessen.
Sie wird intern mit $V_{CCR} = \SI{319}{\milli\volt}$ (constant current regulation factor) skaliert.
Der Faktor $\eta_{XFMR}$ ist der Gesamtwirkungsgrad des Tranformators, es werden $0,85$ angenommen.
Um die Ausgangsleistung trotz schlechterem Wirkungsgrad oder anderen Toleranzen nicht zu stark zu begrenzen
sollte $I_{OCC}$ etwas größer gewählt werden als der erforderliche Ausgangsstrom.

\begin{equation}
    B
    \label{eq:r-cs}
\end{equation}

Der maximale Spitzenstrom ergibt sich nun aus dem Schwellwert $V_{CST(max)} = \SI{740}{\milli\volt}$,
ab dem der Regler in die Strombegrenzung schaltet.

\begin{equation}
    I_{PP(max)} = \frac{V_{CST(max)}}{R_{CS}} = 
    \label{eq:i-pp-max}
\end{equation}

Mit dem Spitzenstrom kann jetzt wie in §Grundlagen LPri§ die Primärinduktivität festgelegt werden.

\begin{equation}
    A
    \label{eq:l-pri}
\end{equation}

%Falsch Zur Spannungsregelung nutzt der UCC28730 die Hilfswicklung, 
Die Hilfswicklung wird zur Versorgung des ICs genutzt.
Da sie gleichzeitig mit der Sekundärwicklung leitet kann hier das Wicklungs- mit dem Spannungsverhältnis berechnet werden.
Als benötigte Spannung wird $V_{VDD(off)} = \SI{7,7}{\volt}$, die Ausschaltschwelle des Reglers angesetzt.
Diese Spannung soll dann unterschritten werden, wenn in der Strombegrenzung die niedrigste erlaubte Ausgangsspannung auftritt.
Bei einer Ausgangsspannung von $V_{OCC} = \SI{10}{\volt}$ (output constant current) soll der Regler ausschalten.

\begin{equation}
    N_{AS} = \frac{V_{VDD(off)} + V_{FA}}{V_{OCC} + V_F} = \frac{\SI{7,7}{\volt} + \SI{0,5}{\volt}}{\SI{10}{\volt} + \SI{0,5}{\volt}} = 0,78
    \label{eq:n-as}
\end{equation}

An dieser Stelle endet die endet die Führung durch das Datenblatt,
es wird die Hinweise aus §Grundlagen beziehugsweise auf die Design Notes §§§ zurückgegriffen.

\section{Auswahl der Wickeldrähte}

Zunächst kann die Strombelastung der ienzelnen Windungen berechnet werden.
Mit der Formel §IRMS aus Ipeak§ ergeben sich die folgenden Werte, $I_{Sek,Eff}$ wird vom Spitzenstrom der Primärseite abgeleitet.

\begin{equation}
    I_{Pri,Eff} = I_{PP,max} \cdot \sqrt{\frac{D_{MAX}}{3}} = \SI{199}{\milli\ampere}
    \label{eq:Ipri-eff}
\end{equation}
\begin{equation}
    \hat{I}_{Sek} = I_{PP,max} \cdot N_{PS,ideal} = \SI{3,22}{\ampere}
    \label{eq:Isek-peak}
\end{equation}
\begin{equation}
    I_{Sek,Eff} = \hat{I}_{Sek} \cdot \sqrt{\frac{1 - D_{MAX}}{3}} = \SI{1,33}{\ampere}
    \label{eq:Isek-eff}
\end{equation}

Anhand einer Stromdichte, die in §Trafo Design Note§ mit $J = \SI{5}{\frac{\ampere}{\milli\meter\squared}}$
empfohlen wird kann jetzt der mindestens benötigte Leiterquerschnitt bestimmt werden.
Aus diesem wiederum ergibt sich der Durchmesser $d$ der Wickeldrähte.
Die Höhe von $J$ ist ein Kompromiss aus Baugröße, Leiterverlusten, Verlusten durch Skin-Effekt und Kosten.

\begin{equation}
    A_{Pri} = \frac{I_{Pri,Eff}}{J}
\end{equation}

\begin{equation}
    A_{Sek} = \frac{I_{Sek,Eff}}{J}
\end{equation}

\begin{equation}
    d_{Pri} = \sqrt{4\frac{A_{Pri}}{\pi}} = \SI{0,225}{\milli\meter}
\end{equation}

\begin{equation}
    d_{Sek} = \sqrt{4\frac{A_{Sek}}{\pi}} = \SI{0,583}{\milli\meter}
\end{equation}

Fällt der Querschnitt, wie bei der Sekundärwicklung, recht groß aus,
so kann auch über die Parallelschaltung mehrerer Wicklungen nachgedacht werden.

\begin{equation}
    d_{Sek,2} = \sqrt{4 \frac{\frac{1}{n} A_{Sek}}{\pi}} = \sqrt{4 \frac{\frac{1}{2} A_{Sek}}{\pi}} = \SI{0,412}{\milli\meter}
    \label{eq:d-sek-multi}
\end{equation}

Mit $n = 2$ Wicklungen ergibt sich ein Querschnitt, von $d_{Sek} \approx \SI{0,4}{\milli\meter}$,
der auch in der Werkstatt vorhanden ist.

Um die Sekundärseite sicher von der Primärseite zu isolieren kann spezielles Isolationsband
eingesetzt werden.
Gerade bei Prototypen eignet sich aber dreifach isolierter Draht (TIW - Triple Insulated Wire).
Dieser hat den Vorteil, dass Unachtsamkeiten beim Wickeln,
aber auch die Nähe zum, als leitfähig angenommenen Kern
die Isolation nicht gefährden.
In der Werkstatt sind TIW lediglich mit Leiterdurchmesser $\SI{0,2}{\milli\meter}$ und $\SI{0,25}{\milli\meter}$ vorhanden.
Es ist anzumerken, dass der Umfang der TIW bei diesem Durchmesser dem Doppelten des Leiterdurchmessers entspricht.
Versucht man die Sekundärseite mit diesen Durchmessern auszuführen, so ergibt sich mit (\ref{eq:d-sek-multi}),
dass $n=5$ parallele Windungen nötig wären um $J \approx \SI{5}{\frac{\ampere}{\milli\meter\squared}}$ herzustellen.
Deshalb wurde nicht die Sekundärseite, sondern die Primärseite, also Primär- und Hilfswicklung, mit TIW ausgeführt.
Das hat Konsequenzen für die Isolationsabstände zwischen Kern und Bauteilen, da der Kern jetzt zur Sekundärseite gezählt werden muss.
Für diese Arbeit soll die Isolation aber ausreichend sein.
Die gewählten Drähte sind in Tabelle §XXXXXXXXXXXX§ aufgelistet.

\section{Auswahl des Transformatorkerns}
Die Auswahl des Kerns hängt mit vielen Variablen zusammen und ist unterliegt daher fast zwangsläufig mehreren Iterationen.

\subsection{Größe}
Grundlegend muss die Ungleichung REFXXXX erfüllt sein um sicherzustellen, dass der Kern nicht sättigt, was eine Energieübertragung verhindern würde.

Vor allem $A_e$ ist dabei von der Kerngröße abhängig.
Die Windungen müssen dann im Windungsfenster des Kerns beziehungsweise des Wickelkörpers verlegt werden.
Hier zeigt sich die Komplexität der Auswahl:
Ein größerer Kern bietet mehr Platz wir Windungen, durch seinen größeren Querschnitt $A_e$ senkt er aber die Anzahl der benötigten Windungen insgesamt.
Zusätzlich wird die Suche nach einem guten Startpunkt dadurch erschwert, dass Tabellen und Programme von exakter, maschineller Fertigung ausgehen.
Die Wicklung von Hand benötigt mehr Platz, wodurch ein größeres Wicklungsfenster benötigt wird.

So stellte sich der zuerst bestellte E16-Kern als viel zu klein heraus um die benötigten $\approx 105$ Windungen der Primärseite unterzubringen.
Die Software Frenetic, auf die in §XXXXXX§ genauer eingegangen wird, wurde ein RM8-Kern empfohlen.
Dieser ist etwas größer als der E16-Kern, bietet aber surch seine runde Form eine bessere Ausgangsform für das Wickeln.
Die Auswahl wurde auch dadurch begünstigt, dass RM8-Kerne im Labor auf Lager waren und nicht während des Bestellstopps beschafft werden mussten.
Mit refXXXXXXX ergibt sich eine Mindestwicklungszahl von $N_{Pri,min} = 37$, erheblich weniger als mit dem kleineren E16-Kern.

\subsection{Material}
Ist die Form des Kerns ausgewählt, so kann ein Material gewählt werden.
Die verschiedenen Ferrite sind auf verschiedene Frequenzen ausgelegt, genauere Informationen bieten Tabellen und Datenblätter der Hersteller.
In diesem Fall kommt $N49$ zum Einsatz, auch weil es im Verfügbar ist.

\subsection{Luftspalt}
Im entsprechenden Datenblatt §CITE§ sind die $A_L$-Werte der verschiedenen Matierialien zu finden.
Für manche Kerne sind vorgefertigte Luftspalte verfügbar, für sie ist ein gesonderter $A_L$-Wert angegeben.
Werksseitig eingefügte Luftspalte sind auf das mittlere Bein des Kerns begrenzt.
Dadurch kommt es zu weniger Streuung des B-Feldes, die umliegenden Wicklungen dämpfen dieses zusätzlich.
Da kein Kern mit Luftspalt vorhanden ist muss er, mehr oder weniger, improvisiert werden.

Zunächst muss aus den Werten im Datenblatt ein $A_L$-Wert abgeschätzt werden, mit dem die benötigte Windungszahl berechnet werden kann.

\begin{equation}
    D
\end{equation}

Der Werkstattmeister Herrn Rall gab den wertvollen Tipp:
\begin{quote}
    Dimensionieren Sie ihre Primärwicklung lieber so über, dass Sie den gesamten Wickelkörper gleichmäßig ausfüllen,
    so bekommen Sie eine saubere Fläche um weiter zu arbeiten.
\end{quote}

Entsprechend wurde während des Wickelns der innenliegenden Primärwicklung die Windungszahl so lange erhöht,
bis der Draht sauber auf den entsprechenden Pin gelegt werden konnte.
Aufgrund der Dicke des Wickeldrahte konnte die Mindestwicklungszahl von §XXX nicht in der zweiten Lage erreicht werden.
Deshalb wurden eine dritte und vierte Lage ausgeführt, die Die Wicklungszahl schlussendlich auf $N_{Pri} = 74$ erhöhten.
Die Sekundär- und Hilfswindungszahlen wurden entsprechend den Verhältnissen angepasst.
Eine Auflistung der Windungszahlen und verwendeten Drähte ist in Tabelle §X§X§X§ zu finden.



Sobald die Primärwicklung erfolgt ist kann mit einem LCR-Meter die Primärinduktivität gemessen und eingestellt werden.
Dies erfolgt über die Vergrößerung des Luftspaltes, was zu einer Verringerung der Induktivität führt.
Durch die, jetzt überdimensionerte Primärwicklungszahl muss der Luftspalt deutlich vergrößert werden.
Das erfolgt durch das Einfügen spezieller Abstandsfolien oder durch Papierblätter, die ungefähr $\SI{0,1}{\milli\meter}$ dick sind.
So werden die Kernhälften auseinander gehalten.
Der so entstehende Luftspalt bildet sich aber nicht nur unter den zentralen Bein, sondern tritt an allen Beinen, also drei Mal auf.
In diesem Fall konnte die Induktivität mit zwei Blättern auf jeder Seite und anschließendes zusammenpressen mit den Halteklammern des Kerns
auf $L_{Pri} = \SI{1,22}{\milli\henry}$ eingestellt werden, was einer Abweichung um $\SI{3}{\percent}$ entspricht.
Der Luftspalt ist allerdings größer als es für die Funktion nötig wäre.
In einer weiteren Iteration könnte versucht werden die dritte und vierte Lage zu eliminieren.



%Der Luftspalt wird benötigt um die Energiespeicherfähigkeit des Transformators zu erhöhen.
%In einem regulären Tranfsormator oder Flusswandler sollte ein Luftspalt vermieden werden, da er die Effizienz senkt.
%Für den Sperrwandler
