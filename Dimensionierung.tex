\chapter{Dimensionierung}\label{chap:dimensionierung}

In diesem Kapitel werden die Anforderungen an die einzelnen Komponenten ermittelt.
An zentraler Stelle steht dabei der Schaltregler 'UCC28730' von Texas Instruments.

\section{Schaltregler {UCC28730}}

Für die Auswahl des 'UCC28730' spielen viele Faktoren eine Rolle.
Die vorgesehene Beschaltung ist in Abb. \ref{fig:ic-flyback} zu sehen.

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=0.9\linewidth]{Bilder/Mit-IC-Flyback.pdf}
        \caption{Beschaltung des Schaltreglers {UCC28730}}
        \label{fig:ic-flyback}
    \end{center}
\end{figure}

Der Schaltregler muss seinen Betrieb eigenständig aufnehmen.
Weil die Hilfsspannung vor dem ersten Schaltzyklus noch nicht anliegt,
ist dafür eine spezielle Beschaltung notwendig.
Der 'UCC28730' hat dafür eine Schaltung integriert, die nach dem Starten abschaltet.
Sie ist am HV-Pin bis zu \SI{700}{\volt} ausgelegt, genügt also für $V_{DC,max} = \SI{565}{\volt}$.

Viele Schaltregler-\acp{IC} haben einen \ac{MOSFET} integriert, um die Komplexität der Schaltung zu verringern.
Diese integrierten \acp{MOSFET} sind aber typischerweise nicht für Durchbruchspannungen über \SI{800}{\volt} ausgelegt.
Aufgrund der hohen Spannungen wird in Abschnitt \ref{ssec:dim-fet} ein diskreter \ac{MOSFET} ausgewählt.
Um diesen zu treiben, besitzt der 'UCC28730' eine Treiberschaltung.

Um im \ac{QR}-Modus zu arbeiten, muss der \ac{IC} die Spannung am \ac{MOSFET} überwachen.
Dafür wird die Spannung der Hilfswicklung über einen Spannungsteiler erfasst.
Zur Stromregelung kommt zusätzlich der Strommesswiderstand $R_{CS}$ zum Einsatz.
Über die Modulation von Frequenz und Strom verspricht der Hersteller einen effizienten Betrieb über
den gesamten Eingangsspannungs- und Lastbereich \cite{UCC28730}.


\section{Gleichrichter und Zwischenkreis}\label{sec:dim-acdc}
Mit der hier umgesetzten passiven Gleichrichtung ist abzusehen,
dass der Leistungsfaktor der Schaltung sehr schlecht ausfällt.
Dies könnte durch ein Netzfilter verbessert werden.
Durch diesen könnte auch verhindert werden, dass hochfrequente Störungen in das Netz eingebracht werden.
Die Umsetzug eines solchen Filters liegt nicht im Rahmen dieser Arbeit.

\subsection{Gleichrichter}\label{sec:dim-fbr}

Es kommt ein Brückengleichrichter zum Einsatz.
Dieser muss den Spitzenwert der Eingangsspannung blocken können.

\begin{equation}
    U_P = \sqrt{2} \cdot U_{AC,max} = \SI{565}{\volt}
\end{equation}

Des weiteren sollte seine Vorwärtsspannung $U_F$ einen möglichst geringen Wert aufweisen um Verluste zu vermeiden.
Der gewählte Gleichrichter 'MDB10S-HF' weißt mit $U_{RMS} = \SI{700}{\volt}$ und $U_F = \SI{0,8}{\volt}$ bei \SI{100}{\milli\ampere}
beide Eigenschaften auf.
% https://eu.mouser.com/datasheet/2/80/B05S_HF_Thru408783__B10S_HF_RevC-2504752.pdf


\subsection{Zwischenkreiskapazität}\label{sec:dim-Cbulk}

\subsubsection{Auslegung der Kapazität}

Die Spannweite der Zwischenkreisspannung $U_{DC}$ kann wie folgt berechnet werden.
Das Maximum $U_{DC,max}$ ergibt sich aus dem Spitzenwert der gleichgerichteten Netzspannung.

\begin{equation}
    U_{DC,max} = \sqrt{2} \cdot U_{AC,max} = \SI{565}{\volt}
    \label{eq:U_DC,max}
\end{equation}

Der Minimalwert der Zwischenkreisspannung $U_{DC,min}$ ist davon abhängig, wie weit der Pufferkondensator $C_{Bulk}$ während einer Halbwelle entladen wird.
Je größer $C_{Bulk}$ ist, desto weniger bricht die Spannung während des Entladens ein.
Ein beispielhafter Verlauf ist in Abb. \ref{fig:U-Bulk} dargestellt.

\begin{figure}[h]
    \centering
    \input{Bilder/VbulkPlot.tex}
    \caption{Zwischenkreisspannung bei Wechselspannung}
    \label{fig:U-Bulk}
\end{figure}

$U_{DC,min}$ kann mit folgender Formel aus \cite{Infineon-QR-2-9} ermittelt werden.
Der Ladezyklus $d_{charge} = \frac{T_{Charge}}{T}$ kann dabei mit $0,2$ angenähert werden.
$P_{in,max}$ entspricht dem Quotienten aus maximaler Nutzleistung und erwartetem Wirkungsgrad.

\begin{equation}
    U_{DC,min} = \sqrt{2 \cdot U_{AC,min} ^ 2 - \frac{P_{in,max} \cdot (1 - d_{charge})}{C_{Bulk} \cdot f_{AC}}}
    \label{eq:U_DC,min}
\end{equation}

Anhand von (\ref{eq:U_DC,min}) wird in Abb. \ref{fig:U-DCmin} die Minimalspannung in Abhängigkeit von $C_{Bulk}$ betrachtet.
Es ist zu erkennen, dass eine Erhöhung der Kapazität im niedrigen Bereich einen großen Effekt hat,
der mit zunehmender Größe stark abnimmt.

\begin{figure}[h]
    \centering
    \input{Bilder/CbulkVbulkPlot.tex}
    \caption{$U_{DC,min}$ in Abhängigkeit von $C_{Bulk}$}
    \label{fig:U-DCmin}
\end{figure}

Im Datenblatt des Schaltreglers ist eine detailliertere Gleichung angegeben, um wahlweise das
Spannungsminimum oder die benötigte Kapazität zu errechnen \cite[S.23]{UCC28730}.
Hier wird ein zusätzlicher Faktor $N_{HC}$ verwendet, um ausfallende Halbwellen der Versorgungsspannung zu berücksichtigen.

\begin{equation}
    C_{Bulk} \geq \frac{2 P_{in,max} \cdot (0,25 + 0,5 N_{HC} + \frac{1}{2\pi} \cdot \arcsin(\frac{U_{DC,min}}{\sqrt{2} \cdot U_{AC,min}}))}
                        {(2 U_{AC,min}^2 - U_{DC,min}^2) \cdot f_{AC}}
    \label{eq:dim-Cbulk}
\end{equation}

Durch Ausprobieren beziehungsweise Plotten kann mit der Formel auch $U_{DC,min}$ für eine gegebene Kapazität ermittelt werden.
%?Für $V_{AC,min}$ und $f_{LINE}$ entsprechen \SI{400}{\volt}/\SI{50}{\hertz} aus Tabelle \ref{tab:anforderungen}.

Aufgrund des Bestellstopps der Hochschule mussten die Bauteile bereits vor der exakten Auslegung der Schaltung bestellt werden.
Deshalb werden verhältnismäßig kleine Kondensatoren mit $C_{Bulk} = \SI{6,8}{\micro\farad}$ verwendet.
Wird $N_{HC}$ vernachlässigt, so ist die gewählte Kapazität ausreichend um eine Mindestspannung von
$U_{DC,min} = \SI{90}{\volt}$ zu gewährleisten.
Da der \ac{SST} fest installiert werden soll ist diese Vereinfachung möglich.

Es ist wichtig zu betonen, dass die Versorgungsspannung des Hilfsnetzteils mit steigender Last am \ac{SST} zusätzlich absinken wird.
Tests müssen zeigen, ob ein stabiler Betrieb unter den entsprechenden Bedingungen möglich ist oder ob $C_{BULK}$ vergrößert werden muss.
Wie in Abb. \ref{fig:U-Bulk} zu sehen ist kann in diesem niedrigen Bereich schon eine kleine Erhöhung der Kapazität eine Verbesserung bringen.

\subsubsection{Problematik der hohen Betriebsspannung}\label{ssec:problematikDerHohenSpannung}

Da die die Zwischenkreisspannung mit $U_{DC,max} = \SI{565}{\volt}$ oberhalb der maximalen Betriebsspannung
gängiger Elektrolyt-Kondensatoren liegt, muss die Grundschaltung erweitert werden.
Um die Spannungsanforderung zu erfüllen, werden zwei Kondensatoren in Reihe geschaltet.
Diese Anordnung hat allerdings eine Schwachstelle:

Die Leckströme unterliegen einer großen Toleranz, die nicht genau angegeben werden kann.
Durch unterschiedliche Leckströme in den Kondensatoren kann es nun zu einem Ladungsungleichgewicht kommen.
Ist zum Beispiel der Leckstrom von C1 erheblich größer als der von C2, sammelt sich in C2 mit der Zeit eine immer größere Ladung.
So steigt auch die Spannung über C2.
Kann die erhöhte Spannung das Ungleichgewicht der Leckströme nicht kompensieren, so steigt die Spannung über den zulässigen Bereich hinaus.
Hierdurch kommt es mindestens zu einer stark verkürzten Lebenszeit, im schlimmsten Fall sogar zum Versagen des Kondensators.
Ein beispielhafter, statischer Fall ist in Abb. \ref{fig:doppelKondensator} zu sehen.
Die Spannung des unteren Kondensators wird durch die Zenerdioden geklemmt.

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=0.9\linewidth]{Bilder/Doppelkondensator.pdf}
        \caption{Klemmung der Kondensatorspannungen mit Zenerdioden}
        \label{fig:doppelKondensator}
    \end{center}
\end{figure}

Um dem Ungleichgewicht entgegen zu wirken, gibt es zwei Möglichkeiten.
Die erste besteht in der Einstellung der Spannung am mittleren Knoten über einen Spannungsteiler.
Um ein sicheres Abfließen unausgeglichener Leckströme zu gewährleisten sollte durch den Spannungsteiler ein Vielfaches der Toleranz des Leckstroms fließen.
Die Spannungsfestigkeit der Widerstände ist ebenfalls zu berücksichtigen.
Die zweite Möglichkeit ist, die Spannungen der einzelnen Kondensatoren über Zenerdioden zu klemmen.
So kann sich zwischen den Kondensatoren zwar ein Ungleichgewicht aufbauen, dieses wird aber auf ein sicheres Niveau begrenzt.
Hierbei entstehen im Idealfall keine zusätzlichen Verluste, allerdings höhere Kosten, da Zenerdioden kostenintensiver sind als Widerstände.
Die Toleranz der Zenerdioden ist zu beachten.

An den gewählten $U_Z = \SI{180}{\volt}$ Zenerdioden, von denen insgesamt vier in Reihe geschaltet werden, entsteht im Worst-Case eine Verlustleistung von

\begin{equation}
    P_{Z,max} = I_{Leak,max} \cdot U_Z = \SI{81,6}{\micro\ampere} \cdot \SI{180}{\volt} \approx \SI{15}{\milli\watt}
    \label{eq:verluste-zener}
\end{equation}

Eine Betrachtung der Verluste bei Verwendung eines Spannungsteilers ist in \cite{AN2872-HVBuck} zu finden, die Verluste bei hoher Spannung liegen dort bei $\approx\SI{150}{\milli\watt}$.


\section{Speichertransformator}
\subsection{Elektrische Eigenschaften}

In diesem Abschnitt wird nach den Vorgaben im Datenblatt \cite{UCC28730} des gewählten Schaltreglers\break 'UCC28730' vorgegangen.

Zuerst wird hier der maximale Duty-Cycle $D_{MAX}$ berechnet.
Er wird durch den maximalen Entmagnetisierungszyklus von $D_{MAGCC} = 0,432$
und eine Halbwelle der Oszillation entsprechend Abschnitt \ref{ssec:grundlagen-qr} begrenzt.
Für deren Abschätzung wird eine Frequenz von $\SI{500}{\kilo\hertz}$ empfohlen, was einer Periodendauer von $t_R = \SI{2}{\micro\second}$ entspricht.
Sie wird mit der maximalen Schaltfrequenz des Reglers von $f_{MAX} = \SI{83}{\kilo\hertz}$ normiert.

\begin{equation}
    D_{MAX} = 1 - D_{MAGCC} - (\frac{t_R}{2} \cdot f_{sw,max}) = 0,485
    \label{eq:dim-Dmax}
\end{equation}

Nun kann das Windungsverhältnis $N_{PS(ideal)}$ über das Spannungszeitflächengleichgewicht berechnet werden.

Für die Spannung der Sekundärwicklung wird die Summe aus
$U_{out}$, der regulären Ausgangsspannung und
$U_F$, der Vorwärtsspannung der Gleichrichterdiode gebildet.

\begin{equation}
    N_{PS(ideal)} = \frac{N_{Pri}}{N_{Sek}} = \frac{D_{MAX} \cdot U_{DC,min}}{D_{MAGCC} \cdot ( U_{out} + U_F)}
                    = 6,5
    \label{eq:NpsIdeal}
\end{equation}

Aufgrund der internen Reglerstruktur des \acp{IC} wird der Spitzenstrom $I_{PP}$ über den
Strommesswiderstand $R_{CS}$ bestimmt.
Dieser begrenzt den Ausgangsstrom auf $I_{out,max}$,
der über das Windungsverhältnis auf der Primärseite sichtbar und mit $R_{CS}$ gemessen wird.
Die abfallende Spannung wird intern mit $U_{CCR} = \SI{319}{\milli\volt}$ skaliert.
Der Faktor $\eta_{XFMR}$ ist der Gesamtwirkungsgrad des Transformators, für den $0,85$ angenommen werden.
Um die Ausgangsleistung trotz schlechterem Wirkungsgrad oder anderen Toleranzen nicht zu stark zu begrenzen
sollte $I_{out,max}$ etwas größer gewählt werden als der erforderliche Ausgangsstrom.
Hier sei $ I_{out,max} = \SI{600}{\milli\ampere}$.

\begin{equation}
    R_{CS} = \frac{U_{CCR} \cdot N_{PS}}{2 \cdot I_{out,max}} \cdot \sqrt{\eta_{XFMR}}
            = \SI{1,5}{\ohm}
    \label{eq:r-cs}
\end{equation}

Der maximale Spitzenstrom ergibt sich nun aus dem Schwellwert $U_{CST(max)} = \SI{740}{\milli\volt}$,
ab dem der Regler in die Strombegrenzung schaltet.

\begin{equation}
    I_{PP,max} = \frac{U_{CST(max)}}{R_{CS}} = \SI{0,494}{\ampere}
    \label{eq:i-pp-max}
\end{equation}

Mit dem Spitzenstrom kann jetzt die Primärinduktivität $L_{Pri}$ festgelegt werden.

\begin{equation}
    L_{Pri} = \frac{2 \cdot (U_{out} + U_F) \cdot I_{out,max}}
                    {I_{PP,max} \cdot f_{sw,max} \cdot \eta_{XFMR}}
            = \SI{1,185}{\milli\henry}
    \label{eq:l-pri}
\end{equation}

%Falsch Zur Spannungsregelung nutzt der UCC28730 die Hilfswicklung, 
Die Hilfswicklung wird vom Schaltregler zur Regelung der Ausgangsspannung und für dessen Versorgung genutzt.
Da sie gleichzeitig mit der Sekundärwicklung leitet kann hier das Wicklungs- mit dem Spannungsverhältnis berechnet werden.
Als mindestens benötigte Spannung wird $U_{VDD(off)} = \SI{7,7}{\volt}$, die Ausschaltschwelle des Reglers angesetzt.
Diese Spannung soll dann unterschritten werden, wenn in der Strombegrenzung die niedrigste erlaubte Ausgangsspannung auftritt.
Bei einer Ausgangsspannung von $U_{out,min} = \SI{6}{\volt}$ soll der Regler ausschalten.

\begin{equation}
    N_{AS} = \frac{N_{Aux}}{N_{Sek}} = \frac{U_{VDD(off)} + U_{FA}}{U_{out,min} + U_F} = \frac{\SI{7,7}{\volt} + \SI{0,5}{\volt}}{\SI{6}{\volt} + \SI{0,5}{\volt}} = 1,26
    \label{eq:n-as}
\end{equation}

\begin{equation}
    N_{PA} = N_{PS} \cdot \frac{1}{N_{AS}} = 5,16
\end{equation}

Nun können noch die Leiterdurchmesser der Wickeldrähte bestimmt werden.
Dafür wird zuerst die Strombelastung der einzelnen Windungen bestimmt.
Nach \cite[S.9]{Infineon-DCM-2-8} ergeben sich die folgenden Werte, $I_{Sek,Eff}$ wird dabei vom Spitzenstrom der Primärseite abgeleitet.

\begin{equation}
    I_{Pri,Eff} = I_{PP,max} \cdot \sqrt{\frac{D_{MAX}}{3}} = \SI{199}{\milli\ampere}
    \label{eq:Ipri-eff}
\end{equation}
\begin{equation}
    \hat{I}_{Sek} = I_{PP,max} \cdot N_{PS,ideal} = \SI{3,22}{\ampere}
    \label{eq:Isek-peak}
\end{equation}
\begin{equation}
    I_{Sek,Eff} = \hat{I}_{Sek} \cdot \sqrt{\frac{1 - D_{MAX}}{3}} = \SI{1,33}{\ampere}
    \label{eq:Isek-eff}
\end{equation}

Anhand einer Stromdichte, die in \cite[Abschnitt~2.8]{AN4137-onsemi-Trafo} mit $J = \SI{5}{\frac{\ampere}{\milli\meter\squared}}$
empfohlen wird, kann dann der mindestens benötigte Leiterquerschnitt bestimmt werden.
Aus diesem wiederum ergibt sich der Durchmesser $d$ der Wickeldrähte.
Die Höhe von $J$ ist ein Kompromiss aus Baugröße, Leiterverlusten, Verlusten durch Skin-Effekt und Kosten.

\begin{equation}
    A_{Pri} = \frac{I_{Pri,Eff}}{J}
    \qquad
    A_{Sek} = \frac{I_{Sek,Eff}}{J}
    \label{eq:leiterquerschnitt}
\end{equation}

\begin{equation}
    d_{Pri} = \sqrt{4\frac{A_{Pri}}{\pi}} = \SI{0,225}{\milli\meter}
\end{equation}

\begin{equation}
    d_{Sek} = \sqrt{4\frac{A_{Sek}}{\pi}} = \SI{0,583}{\milli\meter}
\end{equation}

Fällt der Durchmesser, wie bei der Sekundärwicklung, recht groß aus,
so kann auch über die Parallelschaltung mehrerer Wicklungen nachgedacht werden.

\begin{equation}
    d_{Sek,n} = \sqrt{4 \frac{\frac{1}{n} A_{Sek}}{\pi}}
    \qquad
    d_{Sek,2} = \sqrt{4 \frac{\frac{1}{2} A_{Sek}}{\pi}} = \SI{0,412}{\milli\meter}
    \label{eq:d-sek-multi}
\end{equation}

Mit $n = 2$ Wicklungen ergibt sich ein Querschnitt von $d_{Sek} \approx \SI{0,4}{\milli\meter}$,
der in der Werkstatt zur Verfügung steht.

Um die Sekundärseite sicher von der Primärseite zu isolieren, kann Isolationsband eingesetzt werden.
Gerade bei handgewickelten Prototypen eignet sich aber \ac{TIW}.
Dieses hat den Vorteil, dass Unachtsamkeiten beim Wickeln,
aber auch die Nähe zum als leitfähig angenommenen Kern die Isolation nicht gefährden.
In der Werkstatt sind \acp{TIW} lediglich mit Leiterdurchmesser $\SI{0,2}{\milli\meter}$ und $\SI{0,25}{\milli\meter}$ vorhanden.
Versucht man die Sekundärseite mit diesen Drähten auszuführen, so ergibt sich mit (\ref{eq:d-sek-multi}),
dass $n=5$ parallele Windungen nötig wären um $J \approx \SI{5}{\frac{\ampere}{\milli\meter\squared}}$ herzustellen.
Es ist anzumerken, dass der Außendurchmesser der \acp{TIW} bei diesem Durchmesser dem Doppelten des Leiterdurchmessers entspricht.
Deshalb wird nicht die Sekundärseite, sondern die Primärseite, also Primär- und Hilfswicklung, mit \ac{TIW} ausgeführt.
Das hat Konsequenzen für die Isolationsabstände zwischen Kern und Bauteilen, da der Kern jetzt zur Sekundärseite gezählt werden muss.
Die gewählten Drähte sind in Tabelle \ref{tab:trafo-wicklungen} aufgelistet.

\subsection{Magnetische Eigenschaften}

\subsubsection{Auswahl des Transformatorkerns}
Die Auswahl des Kerns hängt mit vielen Variablen zusammen und unterliegt daher fast zwangsläufig mehreren Iterationen.

Grundlegend muss die Ungleichung (\ref{eq:Bmax}) erfüllt sein um sicherzustellen, dass der Kern nicht sättigt, was eine Energieübertragung verhindern würde.

\begin{equation}
    N_{Pri} \ge \frac{L_{Pri} \cdot I_{PP,max}}{B_{max} \cdot A_e}
            \ge \frac{\SI{1,185}{\milli\henry} \cdot \SI{0,494}{\ampere}}{\SI{250}{\milli\tesla} \cdot \SI{64}{\milli\meter\squared}}
            \ge 36,6
    \label{eq:Bmax}
\end{equation}

$A_e$ ist dabei die Querschnittsfläche des Kerns, während $B_{max} \le \SI{300}{\milli\tesla}$ angenommen wird.
Die Windungen müssen dann im Windungsfenster des Kerns, beziehungsweise des Wickelkörpers, verlegt werden.
Hier zeigt sich die Komplexität der Auswahl.
Ein größerer Kern bietet mehr Platz für Windungen, senkt aber durch seinen größeren Querschnitt $A_e$ die Anzahl der benötigten Windungen insgesamt.

Zusätzlich wird die Suche nach einem guten Startpunkt dadurch erschwert, dass Tabellen und Programme von exakter maschineller Fertigung ausgehen.
Die Wicklung von Hand benötigt mehr Platz, wodurch ein größeres Wicklungsfenster benötigt wird.

Ein E16-Kern beispielsweise benötigt $\approx 105$ Windungen, um (\ref{eq:Bmax}) zu entsprechen, bietet aber nicht genug Platz, um diese auszuführen.
Schlussendlich wurde ein RM8-Kern mit $A_e = \SI{64}{\milli\meter\squared}$ gewählt.
Dieser ist größer als der E16-Kern und bietet durch seine Rundheit eine bessere Ausgangsform für das Wickeln.
Mit (\ref{eq:Bmax}) ergibt sich eine Mindestwicklungszahl von $N_{Pri,min} \ge 37$~ und damit erheblich weniger als mit dem kleineren E16-Kern.

\subsubsection{Material und Luftspalt}
Wurde die Form des Kerns festgelegt, so kann passendes Material gewählt werden.
Die verschiedenen Ferrite sind auf verschiedene Frequenzen ausgelegt.
Genauere Informationen dazu bieten Tabellen und Datenblätter der Hersteller.
In diesem Fall kommt das Ferrit 'N49' zum Einsatz, auch weil es im Labor verfügbar ist.

Im entsprechenden Datenblatt \cite{TDK-core} sind die $A_L$-Werte der verschiedenen Materialien zu finden.
Für manche Kerne sind vorgefertigte Luftspalte verfügbar, für die ein gesonderter $A_L$-Wert angegeben ist.
Werkseitig eingefügte Luftspalte sind auf das mittlere Bein des Kerns begrenzt.
Dadurch kommt es zu weniger Streuung des Magnetfeldes, da die umliegenden Wicklungen dieses zusätzlich dämpfen.
Weil kein Kern mit Luftspalt vorhanden ist, muss dieser in Abschnitt \ref{ssec:einstellenDerInduktivitaet} improvisiert werden.

Zunächst muss dafür anhand der Angaben im Datenblatt ein $A_L$-Wert abgeschätzt werden, mit dem die benötigte Windungszahl berechnet werden kann.
Bei einem Luftspalt von \SI{0,1}{\milli\meter} werden $A_L = \SI{400}{\nano\henry}$ angenommen.
Aus dem Erreichen der Primärinduktivität ergibt sich eine weitere Mindestwicklungszahl,
wobei mehr Windungen über die Vergrößerung des Luftspalts und entsprechend eine Reduktion von $A_L$ ausgeglichen werden können.

\begin{equation}
    L = A_L \cdot N^2
    \qquad
    N_{Pri} \ge \sqrt{\frac{L_{Pri}}{A_L}}
            \ge 54,4
    \label{eq:A-L-Wert}
\end{equation}

Beim Wickeln ist es sinnvoll, die Windungszahl so weit zu erhöhen, dass der Wickelkörper voll ausgefüllt ist.
Die tatsächlichen Wicklungszahlen werden deshalb erst in Abschnitt \ref{ssec:wickeln} festgelegt.
Sie sind in Tabelle \ref{tab:trafo-wicklungen} angegeben.

\section{\acs{MOSFET}}\label{ssec:dim-fet}

Für die Auswahl des \acp{MOSFET} ist dessen Durchbruchspannung $U_{DS}$ relevant.
Wird $U_{Spike}$ mit \SI{30}{\percent} der anderen Spannungen angenommen, geht aus (\ref{eq:uQmaxTheo}) folgende Spannung hervor.

\begin{equation}
    U_{DS,min} = U_{DC,max} + U_R \cdot 1,3 =  \SI{864}{\volt}
    \label{eq:dimMOSFET}
\end{equation}

Die Auslegung von \ac{MOSFET} und Snubber überschneiden sich hier.
$U_{Spike}$ kann vom Snubber auf einen höheren oder einen niedrigeren Wert begrenzt werden.
Trotz dem Snubber muss aber genug Abstand zur Durchbruchspannung vorhanden sein.
Deshalb wird ein \ac{MOSFET} mit $U_{DS} = \SI{950}{\volt}$ ausgewählt.

\section{RCD-Clamp/Snubber}\label{sec:dimSnubber}
Die maximale Spannung $U_{DS}$ des \acp{MOSFET} wurde auf \SI{950}{\volt} festgelegt.
Zur Sicherheit wird die maximale Spannung zusätzlich auf $\SI{85}{\percent} \cdot U_{DS} \approx \SI{805}{\volt}$ begrenzt.
Nach Abzug von $U_{DC,max}$ bleibt für die Summe aus reflektierter Spannung $U_R$ und $U_{Spike}$ eine Differenz von $U_{Clamp} = \SI{240}{\volt}$.
Zur Berechnung wird das Skript \cite[S.83]{Ulrich-Master-Schaltungstechnik} herangezogen.
Der Spannungsripple $\Delta U_{Clamp}$ im Snubber soll $\SIrange{0,05}{0,1}{} \cdot U_{Clamp}$ betragen,
die Streuinduktivität wird mit $\SI{1}{\percent} \cdot L_{Pri}$ angenähert.

\begin{equation}
    R_{Cl} = \frac{2 U_{Clamp} \cdot (U_{Clamp} - U_A \cdot N_{PS})}{L_{Leak} \cdot I^2_{PP,max} \cdot f_{sw,max}}
            = \SI{278}{\kilo\ohm}
\end{equation}

\begin{equation}
    C_{Cl} = \frac{U_{Clamp}}{R_{Clamp} \cdot f_{sw,max} \cdot \Delta U_{Clamp}}
            = \SI{433}{\pico\farad}
\end{equation}

Da diese Schaltung explizit Verluste erzeugt, sollte die Verlustleistung betrachtet werden.
Der Formfaktor von $R_{Cl}$ muss so gewählt werden, dass dieser nicht überhitzt.

\begin{equation}
    P_V = \frac{U^2_{Clamp}}{R_{Cl}}
        = \SI{207}{\milli\watt}
\end{equation}

Die Diode der RCD-Clamp muss in der Einschaltphase die Summe aus die Zwischenkreis- und Klemmspannung sperren.
\begin{equation}
    U_{D,Clamp} = U_{DC,max} + U_{Clamp} = \SI{805}{\volt}
\end{equation}
Um den sicheren Betrieb zu gewährleisten wird eine \SI{1000}{\volt}-Diode vorgesehen.


\section{Sekundärseite und Ausgangsfilter}

\subsection{Gleichrichterdiode}
Durch das Wicklungsverhältnis $N_{PS}$ wird die Spannungsbelastung der Diode gesenkt.

\begin{equation}
    U_D = U_{DC,max} \cdot \frac{1}{N_{PS}} + U_{out} = \SI{102}{\volt}
\end{equation}

Auf diesen Wert sollte allerdings - wie beim \ac{MOSFET} - eine Reserve von \SI{30}{\percent} addiert werden.

Der Effektivstrom wurde in (\ref{eq:Isek-eff}) mit \SI{1,33}{\ampere} berechnet.

Die bestellte Diode 'SS1H10' hält diese Werte nicht ein, ihre Sperrspannung liegt bei nur \SI{100}{\volt}
und die Strombelastung darf \SI{1}{\ampere} nur in Spitzen überschreiten.
Für die Inbetriebnahme wird diese Diode trotzdem eingesetzt, da die jeweiligen Spitzenwerte nur bei maximaler Last
beziehungsweise maximaler Eingangsspannung auftreten.

In der nächsten Iteration müssen $U_D \ge \SI{130}{\volt}$ und $I_F \ge \SI{1,4}{\ampere}$ gewählt werden.

\subsection{Glättung}\label{ssec:dim-outFilter}

Die Formeln im Datenblatt \cite[S.25f]{UCC28730} geben für die Ausgangskondensatoren sowohl die Kapazität als auch die \ac{ESR} vor.

\begin{equation}
    ESR = \frac{0,33 \cdot \Delta V_{out}}{I_{Sek,peak}} = \SI{5}{\milli\ohm}
\end{equation}
\begin{equation}
    C_{out} = \frac{I_{out,max}}{0,33 \cdot \Delta V_{out} \cdot f_{sw,max}} = \SI{243}{\micro\farad}
\end{equation}

Es werden zwei Elektrolytkondensatoren mit je \SI{150}{\micro\farad} verwendet.
Experimentell wird mit ihnen ein CLC-Filter nach dem Vorbild von \cite[Abschnitt 29.4]{Schlienz-Buch} umgesetzt um den Ripple zusätzlich zu verringern.
Die Induktivität zwischen den Kondensatoren wirkt dabei wie ein zusätzlicher Tiefpass.
Um die \ac{ESR} zu senken wird jeweils ein \SI{10}{\micro\farad} Keramikkondensator parallelgeschaltet.


\section{Spannungsmessung und Versorgung des Schaltreglers}

\subsection{Spannungsmessung}
Für die Überwachung der Ausgangsspannung und die Versorgung des \acp{IC} wird die Hilfswicklung $N_{Aux}$ eingesetzt.
Ihr Wechselsignal bildet über die jeweiligen Windungsverhältnisse in der Einschaltphase die Zwischenkreisspannung
und in der Ausschaltphase die Summe aus Ausgangsspannung und Vorwärtsspannung der Diode ab.
Diese Werte werden über einen Spannungsteiler vom 'UCC28730' am VS-Pin gemessen.
Für die Erfassung von $U_{Bulk}$ wird VS mit der Masse verbunden und der Strom durch $R_{S1}$ gemessen.
Liegt dieser über dem Wert $I_{VSL(run)} = \SI{225}{\micro\ampere}$ wird der Regelbetrieb nicht aufgenommen.
Für eine sichere Inbetriebnahme wurde der Wert von $U_{DC,run}$ nicht auf die empfohlenen $\sqrt{2} \cdot U_{AC,min}$,
sondern auf \SI{90}{\volt} festgelegt.

\begin{equation}
    R_{S1} = \frac{U_{DC,run}}{N_{PA} \cdot I_{VSL(run)}} = \SI{77,5}{\kilo\ohm}
\end{equation}

Mit $R_{S2}$ wird dann die Ausgangsspannung festgelegt. Die Referenzspannung des Reglers beträgt $U_{VSR} = \SI{4,04}{\volt}$

\begin{equation}
    R_{S2} = \frac{R_{S1} \cdot U_{VSR}}
                    {N_{AS} \cdot (U_{out} + U_F) - U_{VSR}} = \SI{20,2}{\kilo\ohm}
\end{equation}

\subsection{Versorgung des Schaltreglers}

Um den \ac{IC} mit einer Gleichspannung zu versorgen, kommen - wie auf der Sekundärseite - eine Gleichrichterdiode und ein Kondensator zum Einsatz.
Aufgrund des Windungsverhältnisses fällt die Sperrspannung größer aus.

\begin{equation}
    U_{DA} = U_{DC,max} \cdot \frac{1}{N_{PA}} + U_{out} \cdot N_{AS} = \SI{128}{\volt}
\end{equation}

Der Strombedarf des \acp{IC} liegt bei $I_{run} = \SI{2,1}{\milli\ampere}$, für das Treiben des \acp{MOSFET} wird \SI{1}{\milli\ampere} angesetzt.
Der Vorwärtsstrom der Diode ist damit unerheblich.

Hier wird die gleiche Diode verwendet wie auf der Sekundärseite.
Es gelten die gleichen Einschränkungen,
ein sicherer Betrieb erfordert eine Sperrspannung von $\ge \SI{150}{\volt}$.

Aufgrund des geringen Stombedarfs reicht hier ein Keramikkondensator zur Glättung aus.

Die für die Kapazität ausschlaggebende Gleichung bestimmt $C_{VDD}$ aus dem Strom $I_{WAIT} = \SI{75}{\micro\ampere}$, der im Leerlauf vom \ac{IC} benötigt wird.
Die Betriebsspannung soll zwischen den Schaltzyklen mit $f_{sw,min} = \SI{32}{\hertz}$ um nicht mehr als $\Delta U_{VDD} = \SI{1}{\volt}$ fallen.

\begin{equation}
    C_{VDD} \ge \frac{I_{WAIT}}{\Delta U_{VDD} \cdot f_{sw,min}} \ge \SI{2,34}{\micro\farad}
\end{equation}

Aus Gründen der Verfügbarkeit wird ein \SI{10}{\micro\farad} Kondensator eingesetzt.
Zur Verbesserung des Frequenzgangs wird ein kleinerer \SI{330}{\nano\farad} Kondensator parallelgeschaltet.

Um eine möglichst exakte Stromregelung über einen breiten Eingangsspannungsbereich zu gewährleisten wird der Widerstand $R_{LC}$ eingefügt.
$K_{LC} = 25,3$ ist reglerspezifisch.
Für die Ausschaltverzögerung $t_D$ des \acp{MOSFET} werden \SI{50}{\nano\second} angenommen.

\begin{equation}
    R_{LC} = \frac{K_{LC} \cdot R_{S1} \cdot R_{CS} \cdot N_{PA} \cdot t_D}{L_{Pri}} = \SI{907}{\ohm}
\end{equation}








%In \cite{Infineon-QR-2-9} wird Tabelle \ref{tab:Cbulk-empfehlung} zur Auslegung von $C_{Bulk}$ angegeben,
%die als Ausgangspunkt dienen kann.
%, im Vergleich zu Tabelle \ref{tab:Cbulk-empfehlung},
%\begin{table}[h]
%    \begin{center}
%        \begin{tabular}{|c|c|}
%            \hline
%            Effektivspannung $V_{AC}$ & Kapazität \\
%            \hhline{|=|=|}
%            \SI{115}{\volt} & \SI{2}{\micro\farad} / \si{\watt} \\
%            \hline
%            \SI{230}{\volt} & \SI{1}{\micro\farad} / \si{\watt} \\
%            \hline
%            \SIrange{85}{265}{\volt} & \SIrange{2}{3}{\micro\farad} / \si{\watt} \\
%            \hline
%        \end{tabular}
%        \caption{Empfehlung für Zwischenkreiskapazität aus \cite{Infineon-QR-2-9}}
%        \label{tab:Cbulk-empfehlung}
%    \end{center}
%\end{table}