\chapter{Realisierung}\label{chap:realisierung}
% Was wurde wie physisch umgesetzt?
% Software, Schaltplan, Layout, Trafo, Löten, Kosten
\section{Schaltplan}

\subsection{EDA-Software}\label{sec:eda}
%aus praktikum
Als \ac{EDA}-Software kommt KiCad zum Einsatz.
KiCad wurde gewählt, da es freie und quelloffene Software ist,
deren Betrieb keine Lizenz erfordert.
Sie enthält weniger fortgeschrittene Features,
ermöglicht so aber einen einfacheren Einstieg und
eine flachere Lernkurve als vergleichbare Programme.

Die Symbole und Footprints der Bauteile stammen aus der umfangreichen Bibliothek von KiCad,
vom Distributor Mouser Electronics oder werden selbst nach den Angaben in den Datenblättern erstellt.
Insbesondere die Footprints müssen in jedem Fall kontrolliert werden,
da ein falscher Footprint die gesamte Platine unbrauchbar machen kann.
Symbol und Footprint des RM8-Transformators werden von Prof. Ulrich bereitgestellt.

\subsection{Schaltplan}
Einige Werte/Bauteile wurden bereits festgelegt bevor alle Details über die Schaltung bekannt waren.
Eine Nachbeschaffung der korrekten Bauteile war zum Zeitpunkt der Korrektur
nicht mehr möglich, wodurch es zu Abweichungen zwischen Kapitel \ref{chap:dimensionierung} Dimensionierung und Schaltplan kommt.
Die Angaben in \ref{chap:dimensionierung} entsprechen in diesem Fall den korrekten Werten.
Der Aufbau und die Inbetriebnahme werden mit den im Schaltplan abgebildeten Bauteilen durchgeführt.
Konkret betrifft das zum Beispiel den Strommesswiderstand $R110$, der hier auf einen maximalen Strom von 500mA dimensioniert wurde.

Der gesamte Schaltplan ist in Anhang \ref{ap:schaltplan} zu finden.

\subsection{Primärseite}
\begin{figure}[h]
    \begin{center}
        \includegraphics[width=0.8\linewidth]{Bilder/schaltplanPri.png}
        \caption{Beschaltung des Schaltreglers}
        \label{fig:schaltplanPri}
    \end{center}
\end{figure}
Als Ein- und Ausgänge werden Headerpins verwendet.
Pro Pin wird dabei von einer Stromtragfähigkeit von maximal \SI{500}{\milli\ampere} ausgegangen.
Am Ausgang werden deshalb zwei Pins vorgesehen.
Der Leistungsfluss findet von links nach rechts statt.

Ganz links zuerst der Gleichrichter und die in Abschnitt \ref{ssec:problematikDerHohenSpannung} beschriebene Reihenschaltung der Elektrolytkondensatoren
sowie deren Klemmung durch Zenerdioden.
Die Zwischenkreisspannung soll zusätzlich durch den Keramikkondensator C117 gepuffert werden.
Aufgrund der geringeren \ac{ESR} dient er als Bypass für höhere Frequenzen, die durch das Schalten des \acp{MOSFET} entstehen.
Weil die Kapazität von Keramikkondensatoren mit anliegender Gleichspannung sinkt, wird C117 mit einer Nennspannung von \SI{1000}{\volt} dimensioniert.

Danach die Snubberschaltung in Abb. \ref{fig:schaltplanPri}, die hier auf einen erheblich kleineren Strom eingestellt wurde als in Abschnitt \ref{sec:dimSnubber}.
Sie wird durch einen zweiten Widerstand erweitert.
Das halbiert zum einen die Verlustleistung pro Widerstand und senkt somit die Temperatur, zum anderen wird so die Spannungsfestigkeit der Widerstände gesichert.
Abhängig von ihrer Bauform sind \acs{SMD}-Widerstände nur bis zu bestimmten Spannungen geeignet.
Bei R102 und R103 sind das jeweils \SI{150}{\volt}, also weniger als die Klemmspannung von \SI{240}{\volt}.

Wie bei den Widerständen des Snubbers muss auch bei $R_{CS}$ beziehungsweise R110 auf die Verlustleistung geachtet
und gegebenenfalls eine größere Bauform gewählt werden.
Zum Zeitpunkt des Platinenentwurfs wurde ein 0805-Widerstand als ausreichend angenommen,
in einer weiteren Iteration sollte aber eine größere Bauform eingesetzt werden.

Für die tatsächliche Beschaltung des Schaltreglers dient der zugehörige User's Guide \cite{EVM-552-UCC28730} als zusätzliche Orientierung.
Aufgrund des geringen Stroms von \SI{250}{\micro\ampere} in den HV-Pin wird der Widerstand $R105$ keinen relevanten Spannungsabfall erzeugen.
Er kann aber die Gefahr eines Kurzschlusses begrenzen.
Der \ac{MOSFET} wird nicht wie in Abb. \ref{fig:ic-flyback} direkt an den DRV-Pin angeschlossen, sondern es wird ein Netzwerk aus Widerständen und Diode eigefügt.
Über dieses kann das Schaltverhalten des \acp{MOSFET} verändert werden.
Durch die Diode kann das Sperren im Vergleich zum Einschalten beschleunigt werden.
Weil das nur notwenig wird, falls Störungen auftreten, wird die Diode D105 nicht und R105 als Brücke bestückt.
Obwohl das Datenblatt kein Filter am Current-Sense-Pin vorsieht, wird in \cite{EVM-552-UCC28730} hier mit $R_{LC}$ ein Tiefpass ausgeführt.
Seine Grenzfrequenz liegt bei \SI{8}{\mega\hertz}.
Es sollen also nur sehr hochfrequente Störungen gefiltert werden.
Dieses Filter wird hier ebenfalls ausgeführt, $R_{LC}$ wurde ursprünglich mit \SI{2,2}{\kilo\ohm} dimensioniert.
Weil die Kapazität hier für die Grenzfrequenz relevant ist, wird das Material NP0 ausgewählt.
So unterliegt die Kapazität von C110 keinem DC-Bias.

\begin{equation}
    f_G = \frac{1}{2 \pi R C} = \SI{8}{\mega\hertz}
    \qquad
    C110 = \frac{1}{2 \pi R_{LC} f_G} = \SI{9}{\pico\farad}
\end{equation}


Das Symbol des Speichertransformators wird nach Bedarf verändert.
Weil dieser selbst gewickelt wird, kann die Belegung der Pins so erfolgen, wie es für das Layout Sinn ergibt.
Der Kern wird von zwei Blechklammern zusammengepresst.
Diese werden zur Befestigung an der Platine mit Pin 13 und 14 angelötet.
So kann eine Erdung des Kerns erfolgen.
Weil die Klammern Durchsteckkontakte besitzen, ist es einfacher, diese Pins später mit einer Masse zu verbinden,
als eine bestehende Leiterbahn mit entsprechendem Isolationsabstand aufzutrennen.
Sie werden deshalb vorerst nicht angeschlossen.

\subsection{Sekundärseite}
\begin{figure}[h]
    \begin{center}
        \includegraphics[width=0.8\linewidth]{Bilder/schaltplanSek.png}
        \caption{Wegen UCC24650 veränderte Sekundärseite}
        \label{fig:schaltplanSek}
    \end{center}
\end{figure}
Hier wird stark von der Grundschaltung abgewichen.
Das liegt an der Verwendung des Wake-Up-\acp{IC} 'UCC24650'.
Er wird benötigt um das transiente Verhalten bei Lastsprüngen, insbesondere aus dem Leerlauf, zu verbessern.
Weil der primärseitige Schaltregler die Ausgangsspannung nur unmittelbar nach dem Ausschalten über die Induktion in der Hilfwicklung erfassen kann,
ist es möglich, dass ein sprunghaft gestiegener Laststrom die Ausgangskapazität stark entlädt, bevor die Regelung innerhalb einiger Schaltzyklen darauf reagieren kann.
Hier kommt der Wake-Up-\ac{IC} ins Spiel.
Er überwacht seine Betriebsspannung und sendet bei einem Abfall dieser um \SI{3}{\percent} einen definierten Impuls in die Hilfswicklung.
Dieser wird vom Schaltregler erfasst, der daraufhin unmittelbar mehrere Schaltzyklen mit erhöhter Leistung ausführt, bevor der Regelbetrieb wieder aufgenommen wird.
So kann gegebenenfalls die Ausgangskapazität verringert werden, da das Übertragungsverhalten beschleunigt wird.
Im Bezug auf das Anwendungsziel \ac{SST} ist es wahrscheinlich, dass weder erhebliche Lastsprünge, noch ein Leerlauf überhaupt eintreten, da die DC/DC-Wandler der Halbbrücken im Leerlauf
in Summe \textgreater \SI{100}{\milli\ampere} verbrauchen.

Damit der Wake-Up-\ac{IC} den Impuls in die Sekundärwicklung einbringen kann, muss die Gleichrichterdiode $D104$ in den Massepfad verschoben werden.
Das ist in Abb. \ref{fig:schaltplanSek} zu sehen.
Das hat keine Auswirkung auf die Funktion der Schaltung.
Parallel zur Diode wird ein RC-Snubber vorgesehen, aber nicht bestückt.
Sollte es bei der Inbetriebnahme zu Spannungsspitzen kommen kann, er nachgerüstet werden.

Die Glättung der Ausgangsspannung erfolgt, wie in Abschnitt \ref{ssec:dim-outFilter} beschrieben.

Unten rechts im Schaltplan ist ein 5V-Buck Converter eingezeichnet.
Er wird für den \ac{SST} benötigt und hat keine Bedeutung für diese Arbeit.



\section{Layout} 

Für das Layout werden nach Möglichkeit große 'Hand Soldering' Footprints verwendet, um den späteren Aufbau zu vereinfachen.
Das bedeutet insbesondere bei den Dioden einen größeren Platzbedarf.
Die Größe der Platine spielt aber für den ersten Aufbau eine untergeordnete Rolle.
Relevanter sind die Funktion, zu Zugänglichkeit für Messungen sowie Korrigierbarkeit von möglichen Fehlern.
Deshalb werden viele Testpunkte und alle DNF-Komponenten im Layout vorgesehen.
So könnte während der Inbetriebnahme die Ansteuerung des \ac{MOSFET}-Gates einfach angepasst werden.
Messungen können durch die Testpunkte einfacher und sicherer durchgeführt werden,
als wenn die Testspitzen direkten Kontakt mit der Platine haben müssten.
Ein kompaktes Design steht Flexibilität diametral gegenüber, kann aber, sobald die Funktion sichergestellt ist, optimiert werden.

Da die Platinen bei Aisler bestellt werden sollen werden die entsprechenden Design-Regeln, wie zum Beispiel der Mindestabstand von Leiterbahnen, von dort bezogen.
Diese Grenzwerte sollten zwar generell nicht ausgereizt werden, auf der Primärseite müssen aber weit überschritten werden.
Der Industriestandard IPC-2221 gibt Isolationsabstände für Leiterbahnen mit verschiedenen Spannungsunterschieden vor.
So muss der Isolations-Abstand zwischen den Netzen von Zwischenkreisspannung und Masse beispielsweise mindestens \SI{2,85}{\milli\meter} betragen.
Weil das Netzklassen-System von KiCad nicht fein genug eingestellt werden kann um Abstände zwischen verschiedenen Netzen einzurichten werden die Abstände manuell überprüft.

Für die Beschaltung der \acp{IC} werden die jeweiligen Datenblätter zurate gezogen.
Diese bieten eine detailliertes und geprüftes Design, das nach Bedarf angepasst werden kann.
So wird der Anschluss des HV-Pins hier auf der Unterseite der Platine geführt um die Isolationsabstände zu vergrößern.

Aufgrund der seiner Größe kann der Bypass-Kondensator C117 nicht sinnvoll auf der Vorderseite montiert werden.
Seine Platzierung auf der Rückseite erlaub eine massive Verkleinerung der Masche.
Da mit C117 ohnehin ein Bauteil auf der Rückseite verlötet werden muss ist,
bietet es sich an, weitere Komponenten, dorthin zu verlegen um einen kompakteren Aufbau zu erzielen.
Das ist beispielsweise mit der Beschaltung des \ac{MOSFET}-Gates möglich.

Die seitlichen Kontakte deuten den Formfaktor eines seitlich steckbaren Moduls bereits an, vollenden diesen aber nicht.

Das Layout ist in Anhang \ref{ap:layout} zu finden.
Der Bauraum überschreitet mit $\qtyproduct[product-units = single]{68 x 53 x 20}{\milli\meter}$
die geforderten Maße, enthält aber zusätzlich den \SI{5}{\volt}-Buck Converter.


\section{Aufbau}%/Löten

Nach Sichtprüfung der Platinen werden die Bauteile manuell aufgelötet.
Dabei wird nach Komplexität und Bauteilgröße vorgegangen.
Desto mehr Anschlüsse ein Bauteil hat und desto kleiner es ist, umso früher wird es verlötet.
Zuletzt folgen die Komponenten mit Durchsteckkontakten.

Bei den Halbleiterbauteilen ist auf ausreichenden \ac{ESD}-Schutz zu achten.
Dieser erfolgt durch eine \ac{ESD}-Matte und ein damit verbundenes Erdungsband.
Sie gleichen mögliche Ladungen zwischen Arbeitsplatz und Haut aus.
Durch eine Verbindung der Matte mit der Schutzerde werden sie kontrolliert abgeleitet.
So wird verhindert, dass elektrostatische Entladungen die Bauteile beschädigen können.

Weil dieser zum Zeitpunkt des Aufbaus nicht verfügbar ist kann C117 nicht angeschlossen werden.
Dadurch ist mit größerer elektromagnetischer Abstrahlung der Primärseite zu rechnen.

Um die anschließenden Messungen nicht zu beeinflussen werden der Wake-Up-\ac{IC}, seine Peripherie,
sowie der \SI{5}{\volt}-Buck Converter ebenfalls nicht aufgebaut.

Ein Bild der fertigen Schaltung ist in Abb. \ref{fig:fertigeSchaltung} zu sehen.


\begin{figure}[h]
    \begin{center}
        \includegraphics[width=0.9\linewidth]{Bilder/Fertige Platine.jpg}
        \caption{Bestückte Platine}
        \label{fig:fertigeSchaltung}
    \end{center}
\end{figure}

\section{Speichertransformator}\label{sec:impl-trafo}

\subsection{Wickeln}\label{ssec:wickeln}

Damit die einzelnen Wicklungen sauber übereinander gelegt werden können,
ist es vorteilhaft die erste, unterste Wicklung überzudimensionieren.
Das heißt, es werden mehr Windungen als zwingend notwendig ausgeführt, um so ein insgesamt saubereres Wicklungsbild zu erreichen.

Deshalb wird während des Wickelns der innenliegenden Primärwicklung die Windungszahl so lange erhöht,
bis der Draht wieder sauber aus dem Wicklungsfenster herausgeführt werden kann.
Aufgrund der Dicke des Wickeldrahtes kann die Mindestwicklungszahl von $N_{Pri} \ge 55$ nicht in der zweiten Lage erreicht werden.
Deshalb wird eine dritte und eine vierte Lage ausgeführt, die die Wicklungszahl schlussendlich auf $N_{Pri} = 74$ erhöhen.
Die Sekundär- und Hilfswindungszahlen werden entsprechend der Verhältnisse $N_{PS} = 6,5$ und $N_{AS} = 1,26$ angepasst.
Eine Auflistung der Windungszahlen und verwendeten Drähte ist in Tabelle \ref{tab:trafo-wicklungen} zu finden.

Zum Wickeln kommt die Wickelmaschine der Elektrowerkstatt zum Einsatz.
Diese ist mit einem Umdrehungszähler ausgestattet, so dass sich während der Bedienung
auf das saubere Führen der Drähte konzentriert werden kann.

\subsection{Einstellen der Induktivität}\label{ssec:einstellenDerInduktivitaet}

Sobald die Primärwicklung erfolgt ist, kann mit einem LCR-Meter die Primärinduktivität gemessen und eingestellt werden.
Dies erfolgt über die Vergrößerung des Luftspaltes, was zu einer Verringerung der Induktivität führt.
Die jetzt überdimensionerte Primärwicklungszahl resultiert über den quadratischen Zusammenhang aus
Gleichung (\ref{eq:A-L-Wert}) in einer stark erhöhten Induktivität.
Deshalb muss der Luftspalt deutlich vergrößert werden, um diese auf den gewünschten Wert von $L_{Pri} = \SI{1,185}{\milli\henry}$ einzustellen.
Das erfolgt durch das Einfügen spezieller Abstandsfolien oder durch Papierblätter, die ungefähr $\SI{0,1}{\milli\meter}$ dick sind.
So werden die Kernhälften auseinander gehalten.
Der so entstehende Luftspalt bildet sich aber nicht nur unter dem zentralen Bein, sondern auf der gesamten Breite. 
Die äußeren Beine haben jeweils $\frac{1}{2}A_e$.
Insgesamt entstehen also nur zwei Luftspalte, obwohl drei sichtbar sind.

In diesem Fall konnte die Induktivität mit zwei Blättern auf jeder Seite und anschließendes Zusammenpressen mit den Halteklammern des Kerns
auf $L_{Pri} = \SI{1,22}{\milli\henry}$ eingestellt werden, was einer Abweichung von $\SI{3}{\percent}$ entspricht.
Der Luftspalt ist mit $\approx \SI{0,4}{\milli\meter}$ allerdings erheblich größer, als es für die Funktion nötig wäre.
Das kann zu verschiedenen negativen Effekten, wie dem Streuen des Magnetfelds in Schaltung und Umgebung
oder erhöhten Kernverlusten führen.
In einer weiteren Iteration kann versucht werden die dritte und vierte Lage der Primärwicklung zu eliminieren.

\begin{table}[h]
    \begin{center}
        \begin{tabular}{|l|c|r|c|}
            \hline
            Wicklung & Windungen & Durchmesser & Isolation \\
            \hhline{|=|=|=|=|}
            Primär   & 74 & \SI{0,25}{\milli\meter} & \acs{TIW} \\
            \hline
            Sekundär & 11 & 2 $\cdot$ \SI{0,40}{\milli\meter} & Lack \\
            \hline
            Auxiliar & 14 & \SI{0,20}{\milli\meter}  & \acs{TIW} \\
            \hline
        \end{tabular}
        \caption{Windungszahlen des gewickelten Transformators}
        \label{tab:trafo-wicklungen}
    \end{center}
\end{table}


\section{Kostenkalkulation}

Die Bauteilkosten belaufen sich auf 8,07 \euro\ und sind in Tabelle \ref{tab:bauteilkosten}
genauer aufgeschlüsselt.
Dabei sind auch Bauteile enthalten, die während der ersten Inbetriebnahme nicht zum Einsatz kommen,
wie der Wake-Up-\ac{IC} U101 oder C117.
Die einzelnen Preise wurden am 15.12.2022 abgerufen.

Der Preis der Platine, der in den Anforderungen ausgeschlossen wurde, ist schwierig zu erfassen.
Die Kosten hängen neben der Größe auch maßgeblich vom Finish der Löt-Oberflächen ab.
1000~Platinen mit den Maßen $\qtyproduct[product-units = single]{68 x 52}{\milli\meter}$
bietet der Hersteller Eurocircuits ab einem Stückpreis von 1,37 \euro\ an.


\begin{table}[ht]
    \centering
    \begin{tabular}{|l|l r|c|c|c|}
    \hline
        Bezeichnung & Bauteil & ~ & Anzahl & Einzelpreis & Gesamtpreis \\
        \hhline{|=|==|=|=|=|}
        C101 & Kondensator &\SI{100}{\nano\farad} & 1 & 0,007 & 0,007 \\ \hline
        C102, C104, C112 & Kondensator &\SI{10}{\micro\farad} & 3 & 0,037 & 0,111 \\ \hline
        C103, C105 & Kondensator &\SI{150}{\micro\farad} & 2 & 0,137 & 0,274 \\ \hline
        C106 & Kondensator &\SI{330}{\pico\farad} & 1 & 0,036 & 0,036 \\ \hline
        C107, C109 & Kondensator &\SI{6,8}{\micro\farad} & 2 & 0,323 & 0,646 \\ \hline
        C110 & Kondensator &\SI{9,1}{\pico\farad} & 1 & 0,019 & 0,019 \\ \hline
        C111 & Kondensator &\SI{330}{\nano\farad} & 1 & 0,013 & 0,013 \\ \hline
        C117 & Kondensator &\SI{100}{\nano\farad} & 1 & 1,260 & 1,260 \\ \hline
        D101, D103, D107, D108 & Zenerdiode &\SI{180}{\volt} & 4 & 0,135 & 0,54 \\ \hline
        D102 & Gleichrichterdiode &\SI{1000}{\volt} & 1 & 0,077 & 0,077 \\ \hline
        D104, D109 & Schottkydiode &\SI{100}{\volt} & 2 & 0,112 & 0,224 \\ \hline
        D106 & Brückengleichrichter &\SI{1000}{\volt} & 1 & 0,121 & 0,121 \\ \hline
        L101 & Induktivität &\SI{4,7}{\micro\henry} & 1 & 0,108 & 0,108 \\ \hline
        Q101 & \ac{MOSFET} &\SI{950}{\volt} & 1 & 0,948 & 0,948 \\ \hline
        R101 & Widerstand &\SI{680}{\ohm} & 1 & 0,006 & 0,006 \\ \hline
        R102, R103 & Widerstand &\SI{360}{\kilo\ohm} & 2 & 0,007 & 0,014 \\ \hline
        R105 & Widerstand &\SI{100}{\kilo\ohm} & 1 & 0,007 & 0,007 \\ \hline
        R107 & Widerstand &\SI{5,1}{\ohm} & 1 & 0,013 & 0,013 \\ \hline
        R108 & Widerstand &\SI{2,2}{\kilo\ohm} & 1 & 0,007 & 0,007 \\ \hline
        R110 & Widerstand &\SI{1,96}{\ohm} & 1 & 0,015 & 0,015 \\ \hline
        RS101 & Widerstand &\SI{68}{\kilo\ohm} & 1 & 0,007 & 0,007 \\ \hline
        RS102 & Widerstand &\SI{18}{\kilo\ohm} & 1 & 0,007 & 0,007 \\ \hline
        T101 & Transformator & ~ & 1 & 2,600 & 2,600 \\ \hline
        U101 & Wake-Up-\ac{IC} & ~ & 1 & 0,272 & 0,272 \\ \hline
        U102 & Schaltregler \ac{IC} & ~ & 1 & 0,737 & 0,737 \\
        \hhline{|=|==|=|=|=|}
        ~ & ~ & ~ & ~ & \textbf{Summe:} & \textbf{8,069} \\ \hline
    \end{tabular}
    \caption{Bauteilkosten bei Abnahme von 1000 Stück in \euro}
    \label{tab:bauteilkosten}
\end{table}

