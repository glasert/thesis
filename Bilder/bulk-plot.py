import matplotlib.pyplot as p
from math import *
#from labellines import labelLine, labelLines


duration = 0.02 #s
samples = 200
timePerSample = duration / samples
frequency = 50 #Hz
omega = 2 * pi * frequency
vpeak = 325 #V
load = 150 #W
vAtZeroCrossing = 288

time = []
vrect = []
vbulk = []

loading = False
bulkFile = open(".\VbulkPlot.txt", 'w')
bulkFile.write("Zeit\tSpannung\n")
VinFile = open(".\VinPlot.txt", 'w')
VinFile.write("Zeit\tSpannung\n")

# https://github.com/cphyc/matplotlib-label-lines
for i in range(samples):
    t = i * timePerSample
    time.append(t)
    v = abs(vpeak * sin(omega * (t + 0.01)))
    vrect.append(v)

    if i == 0:
        vbulk.append(vAtZeroCrossing)
    elif v < vbulk[i-1]:
        vbulk.append(vbulk[i-1] - (load / samples))
        loading = False
    elif v >= vbulk[i-1]:
        vbulk.append(v)
        if loading == False:
            loading = True
            print("uponLoad: " + str(vbulk[i-1]) + " V")
            print("uponLoad: " + str(time[i-1]) + " s")
    
    if v > 324:
        print("Vmax: " + str(t) + "s")

    if v <= 0.2:
        print("V at zero crossing:")
        print(vbulk[i])

    bulkFile.write(str(t * 1000) + "\t" + str(vbulk[i]) + "\n")
    VinFile.write(str(t * 1000) + "\t" + str(v) + "\n")



bulkFile.close()
VinFile.close()

p.plot(time, vrect, '--g', time, vbulk, 'b')
p.show()
