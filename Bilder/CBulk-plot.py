import matplotlib.pyplot as p
from math import *

v = []
cap = []

samples = 200

limit = 0.000040
resolution = limit / samples
first = True

CbulkFile = open(".\CbulkPlot.txt", 'w')
CbulkFile.write("C_Bulk\tU_Bulk\n")
for i in range(samples):
    c = i * resolution
    if c != 0:
        a = (2*120*120)
        b = ((8.8235 * (1 - 0.2))/(c * 60))
        if a > b:
            voltage = sqrt(a -b)
            cap.append(c)
            v.append(voltage)

            CbulkFile.write(str(c * 1000000) + "\t" + str(voltage) + "\n")
            if first == True:
                print("lowest Voltage: " + str(voltage) + " V")


CbulkFile.close()

p.plot(cap, v)
p.show()
